# CKC
Columnar (split) Keyboard Configurator

## What is it?
CKC is an [OpenSCAD](https://openscad.org) script that generates a keyboard (the keyswitch holding plastic part) based on a user-supplied configuration script (config.scad). The goal is to make it quick and easy to iterate your keyboard design by just tweaking a few values instead of having to manually design everything from scratch. To ensure that you are _actually able_ to iterate your designs fast CKC is written with render speed as a primary goal - the preview of a five column keyboard side, all with unique curvatures, relative offsets (stagger) and angles takes about a quarter of a second to render on a decade old macbook pro (i7-3520M with 8 GB 1600 MHz DDR3 RAM). The final render on the same machine takes about 15 seconds.

**tl;dr** you provide a config.scad-file and it generates one half of a split keyboard.

## Status
Very much pre-alpha. You can config your columns and thumbcluster (to a certain extent) but not much else. See the [TODO.md](https://codeberg.org/ragnar/ckc/src/branch/main/TODO.md) for a more up to date state of things.

## Docs
They're in the [TODO.md](https://codeberg.org/ragnar/ckc/src/branch/main/TODO.md).. started some docs for the previous version of this project (when it was "just" a keyboard), that's what's in the [CONFG.md](https://codeberg.org/ragnar/ckc/src/branch/main/doc/CONFIG.md) in the [doc](https://codeberg.org/ragnar/ckc/src/branch/main/doc/)-folder, but that's not really relevant anymore..

## Planning
What i originally wanted was just to build a keyboard where i could iterate the designs quickly by having configurable columns (shape, number of keys, rotations, etc) and thumbcluster, and then for the rest (walls and base) be generated automagically. As i started writing it tho, it turned more and more into a keyboard-generator than just some extra config-options for a predefined keyboard - so that's where i am now. I still have very little time to put into it, but i trodd along whenever i get to it. At some point i'll make some more configs to display here but for now i'll keep this not-so-up-to-date version with a semi-random config:
![right hand part with curved columns and an ugly thumbcluster](example.png)

## Motivation
I have broken my wrists, thumbs, and fingers a whole bunch of times, am sort of missing a knuckle and have been writing code on and off for soon 25 years. My hands are also kinda klumpy and wrists are quite unpleasantly kinked when on the home rows on most keyboards. I do have the kinesis advantage and advantage2, which are just wonderful - but i still think there are a few ways they could be improved upon.. hence the idea of a "quick and easy" way to design a keyboard.

## License
Unlicense/public domain