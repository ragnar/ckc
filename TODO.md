# TODO
The stuff that i have planned for coming releases. anything and everything beyond v0.1 is subject to change.

## v0.1a 

- [ ] __columns__, thought i was almost done with these..
  - [x] change the way the _used_ pts are generated, i.e. adjust for flat columns (`cols = []`), column style-options, and alternatives to bezier-points.. this affects (at least):
    - [x] `pts`
    - [x] `[t_]pmi`
    - [x] `co`
    - [x] likely a bunch of other code
    - [x] switch to `cols` containing full paths instead of just bezier pts:
      - [x] example config for both bezier and arcs
      - [x] `pts`, `cols`
      - [x] update `lp`, `[bt]_idxs`
    - [ ] the doc-comments in config.scad
  - [x] fix `front/rear_emc` for flat columns
  - [ ] *if needed* add `fc_col_end_pt`

  - [x] __[rewrite] switch from masking to constructing full sides__
    - [x] replace `pts` such that it includes the returns of `rear/front_emc`
    - [x] update `co[2]` (indices)
    - [x] (done-ish, don't think it'll be needed) store end points in `col_end_pts` and use that instead of calling `end_pts[s]` in subsequent code
    - [x] make switch/caps array with all the props (this will sort of take care of much of the whole "making everything individually configable"-thing)
    - [x] move to solids.scad

  - [ ] __column_joiner__
    - [x] ~~(opt) config `col_merge`: true/false/index pairs (like tc), def: true~~
    - [x] (opt) config `col_merge_w`: scalar/width pairs hull merging width, def: `col_m`
    - [x] ~~just hull the extruded outlines (if it's too slow then deal with it in next version)~~
    - [x] merge with polyhedra at angles..
    - [x] make a few options for column joining (two like an x, trapezoid, etc..)
    - [x] split into separate file that exposes a single function that also takes a "style" parameter.
    - [x] make that style parameter configgable.
    - [ ] make three styles:
      - [ ] "smooth", the one with the extra seem in the middle
      - [ ] "at_length", take the shortest possible overlap of columns and merge that
      - [x] "custom [bunch of options]" (see join_cols.scad)
    - [x] make saner defaults:
      - [x] curved cols with same number of keys: "smooth"
      - [x] otherwise: "at_length"
    - [x] add a tiiiny delta, to move flat sides "into" the cols.


  - [ ] __flat columns__
    - [x] adjust all the code...
    - [x] helpers: `offs` and `angs` generators for tenting
      - [ ] __tenting__
        - [x] tent columns with no rotation (helper that outputs `offs` and `angs`)
        - [ ] deal with rotations, i.e. column length needs to be calced

  - [x] BUG: something's off with the final translation, the lowest point is not quite on the axis it should be..
  - [ ] ISSUE: i think i might have f'd up and not taken into account the difference in width of caps and switches..


- [ ] __thumb cluster__, free positioning with some helpers
  - [x] helper: single row cluster
  - [x] helper: N-row cluster
  - [x] helper: single row cluster along 2D line
  - [x] helper: extend single row cluster along 2D line to a 2.5D-one by adding an initial tilt arg such that you can have the thumb clicking "inwards". that would require 3D SAT and a known depth of the caps+pcb below keys, and so on tho.. or, maybe, that shit's just left up to the user... so just implement it and see.
  - [x] add tc_a/o options and remove the a and o args from the helpers
  - [x] merging config option that merges pairs of keys in the cluster
  - [x] make sure that, at 0 vert offset the top surface aligns with the column top surface!
  - [ ] merging with column(s)!


- [ ] __walls__, start out easy
  - [ ] the outline should be available from the cols and col_joiners
  - [ ] somehow merge nicely with thumbcluster
  - [ ] screw holes


- [ ] __base__, i have barely started
  - [ ] just make a basic projection with configgable thickness
  - [ ] screw holes


- [ ] __switch cutouts__
  - [ ] properly define how they should be oriented and positioned
  - [ ] add the cutout you made for necrapple


- [ ] __code/structure cleanup__
  - [x] split into more files.. solids could become it's own file or even folder
  - [ ] clean up the most egregious use of globals.. some of it should be fixed when splitting into solids-file(s), but maybe make special vars? i dunno, might affect performance.
  - [ ] fix the `use`-ing and `include`-ing.. seems to render multiple times now.
  - [ ] switch to flags in config.scad to control what to render, that way all the cutouts can be diffed with all solids
  - [ ] input validation, config value handling
    - [x] add a validation/parsing section and restructure to have a processing section
    - [ ] validate all the config vars
    - [ ] handle all multivalued config vars
    - [ ] make optional values actually optional (e.g. cols for flats)
    - [ ] maybe add some more "sane defaults"


- [ ] __misc__
  - [ ] add a `switch_h` and work that in to the code... that'll be fun
  - [ ] `cap2cap` needs to be renamed/split into two.. right now it's used as switch2switch (and possibly other things as well)
  - [ ] make sure the left hand thingy works as promised..
  - [x] rename to make CKC, columnar keyboard configurator, and separate the two projects (i.e. make ckc-config for lpcats, and write readme for CKC)

  - [ ] __scalars to vectors/matrices__ (could possibly be moved to next patch version)
    - [ ] possibility to give `btm_keys` as scalar
    - [ ] `col_*` should be possible to give as vectors
    - [ ] per column, or maybe even per key, cutout selection (should of course be possible to give a single cutout for a column, or for all keys) - that way rotary encoders could just be another key.
    - [ ] _maybe_ per-column cap spacing (that shouldn't be too bad)


- [ ] __docs__
  - [ ] look into how codeberg's wiki-engine works, and see if it's possible to export a wiki-site to a standalone document
  - [ ] replace current docs with a wiki-style document
  - [ ] in column joining: REMEMBER to note that the words for custom joins are strncmp'd, so you can separate them with like a comma BUT, as for now it is whitespace sensitive.

  - [ ] __start page__
    - [ ] essentially the readme
    - [ ] note: something about https://pashutk.com/ergopad/ maybe?
    - [ ] note: you always design the right hand side! then you mirror it to make the left!
    - [ ] note: it is unlicensed and you're absolutely free to do whatever you want, but if you create something - anything really: pictures from a build, tips and/or tricks, buildguides, blog posts, diverging forks, or "just" your conf.scad - i would be happy to feature it here, and you really don't have to use git if you don't feel like it, an email to ragnar@dm9.se works just as well.

  - [ ] __config section__
    - [ ] page that lists all config options with short descriptions, grouping them as mandatory and optional
    - [ ] page that show how you can use flat cols for rapid prototyping with cardboard (do layout, project to 2D, export as svg, adjust svg line width, print, cut cardboard, adjust layout, repeat)
    - [ ] curved column overview, use a lot of the current figs and such
    - [ ] curved column examples, one for each relevant helper
    - [ ] flat column examples (inc. tenting and)
    - [ ] thumbcluster overview describing how it is constructed (similar to columns)
    - [ ] thumbcluster examples, one for each helper
    - [ ] walls and base examples. there's really not much to do there now.

    - [ ] __helpers__
      - [ ] overview/index
      - [ ] document and illustrate all the helpers, make a note of how tc_path25d is just tc_path2d with individual angles 

  - [ ] __misc__
    - [ ] note (somewhere not sure where): curves for cols must propagate in pos x-dir
    - [ ] troubleshooting (list errors, their cause, and what to do)
    - [ ] palmrests: notes about rendering only the walls or the [filled] outline


- [ ] __examples__ (or at least approx-versions to begin with) of the more popular designs out there on the interwebs
  - [ ] make an index/overview in the docs
  - [ ] dactyl
  - [ ] sofle
  - [ ] kyria
  - [ ] iris
  - [ ] lily58
  - [ ] mitosis
  - [ ] crkbd (corne)
  - [ ] (something like) avalanche/chocofly, showing how to first generate a curved tc and then, using helpers, add the two buttons "on top" of the two last ones in the curve.


- [x] (get rid of) __dotSCAD-dependence__
  - [x] shape_path_extend, make a stroke-function that has nicely indexed points
  - [x] arc_path, replace with function that can also take two radii (arc of ellipse)
  - [x] bezier_curve
  - [x] function_grapher/sf_thicken, maybe rethink the walls..?
  - [x] loft, making the column joining polygons shouldn't be that hard.. should it?


- [x] fix the placing of the switches
- [x] add configable number of keys per (individual) column
- [x] make index of bottom key configable
- [x] cut off columns after caps are placed
- [x] switch from using indices to calculating new points for the switch cut outs, i.e. fix the cap2cap distance so it's independent of b_stp_sz
- [x] add cap cutouts
- [x] __path helpers__
  - [x] arc with ellipse support (same as dotSCAD dep point, but i needed another checkmark)
  - [x] "line" & line segment
  - [x] rectangle
  - [x] (crude) interpolater
  - [x] rebaser
  - [x] function that takes a function


## v0.1.X

- [ ] __columns__
    - [ ] column-style options: curved (current) or sections (flat sections for each key, angled together from the co-angles). for the latter the used pts gotta be generated from the cutouts.
  - [ ] fix the `6*col_t`-thingy you've got lingering in `fc_len`, use length of `front/rear_emc` instead!
  - [ ] per-switch column width
  - [ ] have a lookiloo if we really need all the indices and whatnot in the columns/cutouts

  - [ ] __column joiner__
    - [ ] make more and better joiners..
    - [ ] make it possible to select which individual columns should be merged

  - [ ] __flat columns__
    - [ ] *maaaaaybe* column wise DXF outputs..
    - [ ] tent by distributing columns along a line (helper that should also generate `offs` and `angs`)


- [ ] __walls__
  - [ ] add option to extend wall in orthogonal direction of columns to make space for caps-bottoms, soldering, etc.
  - [ ] generic cutout-config with helpers for usb-cable, TRRS cable, possibly on-off rocker..
  - [ ] patterned and *maybe* skeleton walls


- [ ] __base__
  - [ ] mounting stuff for electronics
  - [ ] magnet-holder-pockets/cut-outs


- [ ] __switch cutouts__
  - [ ] maybe switch specific cutouts, such as: chocs, gateron LP, regular cherry
  - [ ] others..?


- [ ] __misc__
  - [ ] do some more testing and error checking and whatnot
  - [ ] option to only render (and possibly extrude) the outline (or a "filled outline") of the keyboard, such that people can build their own palmrests (outline to use as a template for cutting, filled outline to use as the negative in a diff of their own 3D-model)


- [ ] __dev docs__ (rewrite the current CONFIG.md)
  - [ ] cover that cols are now paths and not just bezier pts
  - [ ] add section about "manual fitting" of bezier curves using utils.scad
  - [ ] update figures
  - [ ] add the rest of the figures
  - [ ] add sections on the other parts


## v0.2.X

- [ ] __GUI__ (will likely be QT/QML-based)
  - [ ] bundle openscad.. that'll be something.
  - [ ] bezier-drawer-fitter-thingy
  - [ ] component-preview for individual columns, thumb cluster, etc
  - [ ] all the other stuff (this will likely turn into a long list)


- [ ] __base__
  - [ ] *maybe* free shape base..


- [ ] __walls__
  - [ ] full keyboard tilt
  - [ ] add more wall styles (stealth, skeleton)
  - [ ] mounting options


- [ ] __thumb cluster__
  - [ ] merging config option for column ends and cluster keys
  - [ ] support multi-u sized buttons


- [ ] __columns__
  - [ ] third axis rotation (with cols now being full paths the user is free to tilt as xe pleases)


- [ ] get rid of all the stupid atan-crap that's left from the lpcats-days and replace with atan2
- [ ] config option: wireless charging in base
- [ ] profiling and optimizing the code for preview-rendering
- [ ] rotary encoders.. all the cool kids have them these days.
- [ ] add a cap\_w\_at\_top and adjust the cap_dist-calcs accordingly. right now you have play around with the cap2cap-value if you do not have a straight profile cap..

## Possible future stuff

- [ ] config option: magnet mounted wrist rest in walls
- [ ] thumbcluster/helper: add option when using single row cluster along 2D line to fill the line instead of merging to have "soft curves"
- [ ] travel box
- [ ] config option(?): hot-swapper as an addon to the switch-cutouts
- [ ] *maybe* screens.. but that's a big *maybe*. same for trackball/tracknubbin.

