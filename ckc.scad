// This is the "main" part of the implementation of CKC.
// note that main only implies that we generate the variables we need to then
// make solids from. all the modules that generate the solids are located in
// solids.scad and join_cols.scad
//
// Things assumed by this script (that are not noted in the TODO section):
// - That the columns are concave (or flat), if not the switch-cutout placement
//   will almost certainly fail.
//
// Things not taken into account by this script:
// - That the columns are long enough to fit all the switches, if one doesn't the
//   switch cutout will just be sitting out in nothingness
// - If the tilt and rotation of the columns cause the caps/switches to collide
//
// Why only some functions and modules have flat column-counterparts with
// fc-prefixes:
// - because it was an afterthought that came waaaaay after this project had
//   "evolved" from just being my own little keyboard
// - i haven't gotten to writing fc-versions of all of them or they're not needed
//   as the column curvature doesn't matter
//
//

// POSSIBLE TODOs:
// - switch to variables for the rendering and then, if needed, diff the columns
//   with the switch cutouts and caps from the thumbcluster
// - config: tc_merge_with_col, if this one's set we hull them together and have
//   generate the walls as usual. if not, the walls need to make some tunnel or
//   or something.

// this should be renamed to config.scad
include <config_curved_example.scad>;


use <utils.scad>;
use <solids.scad>;


// TODO:
// - argument validation
// - start with a config-parse section where all the opt/multival ones are parsed
// - use better names for variables here all the way down to `columns`

/************************** validation section **************************/
// this is where the config-values are validated and parsed, i.e. scalars are
// turned into lists and so on. the "output" of this should be a consistent set
// of variables that is then used in the next section (variable processing)

/******** validation helpers ********/
// default function checks that each elemnt is >= 0...
function check_scalar_or_list(v, f = function(x) x >= 0) =
	is_list(v) ? [for (e = v) f(e)] == [for (_ = v) true] : f(v);
function check_list(v, f = function(x) x >= 0) = is_list(v) && check_scalar_or_list(v, f);
// ..so we can have a default error message as well
function def_err_msg(x) = str(x, " has to be set to a value (or vector) >= 0");
// similar thing for lists that gotta be the same length
function same_len_if_list(l1, l2) = is_list(l1) ? len(l1) == len(l2) : true;
function samel_len_err_msg(l1, l2) =
	str(l1, " list must contain the same number of elements as ", l2);

// this we need to know to for some validation and defaults
is_curved = is_undef(cols) ? false : true;

/******** mandatory values ********/

// TODO: validate all the mandatory values

assert(check_list(no_keys, function(x) is_num(x) && x > 0),
		"no_keys has to be a list of positive integers");

if (!is_undef(cols)) {
	assert(len(cols) == len(no_keys),
		   "no_keys and cols has to be the same length (if cols is defined)");
}
// TODO: more checking of cols..? convexity maybe?

assert(check_scalar_or_list(col_rem), def_err_msg("col_rem"));
assert(check_scalar_or_list(col_fem), def_err_msg("col_fem"));
assert(check_scalar_or_list(col_m), def_err_msg("col_m"));

/******** optional values ********/ 
// col_merge* section, they all have default values so it's pretty wordy..
// col_merge's default value is false
// use: generate indices of columns selected to be merged
cm_idx = is_undef(col_merge) ? undef :
		 col_merge == false ? undef :
		 col_merge == true ? [for (i = [1:len(no_keys)-1]) [i-1, i]] :
		 is_list(col_merge) ? col_merge :
		 assert(false, "invalid value type for col_merge");

// make sure possible col_merge_w list is valid
if (!is_undef(col_merge_w) && !is_undef(cm_idx))
	assert(same_len_if_list(col_merge_w, cm_idx),
		   same_len_err_msg("col_merge_w",
							"no_keys (if col_merge is bool) or col_merge (if list)"));
// col_merge_w's default value is col_m
// use: set the merge merge widths in the merge_pairs
cm_w = is_undef(cm_idx) ? undef :
	   is_undef(col_merge_w) ? [for (ip = cm_idx) [[ip[0], col_m], [ip[1], col_m]]] :
	   is_list(col_merge_w) ? [for (i = [0:len(cm_idx)-1]) [[cm_idx[i][0], col_merge_w[i][0]],
															[cm_idx[i][1], col_merge_w[i][1]]]] :
	   [for (ip = cm_idx) [[ip[0], col_merge_w], [ip[1], col_merge_w]]];

// check col_merge_style the same way as col_merge_w
if (!is_undef(col_merge_style) && !is_undef(cm_idx)) {
	assert(same_len_if_list(col_merge_style, cm_idx),
		   same_len_err_msg("col_merge_style",
							"no_keys (if col_merge is bool) or col_merge (if list)"));
	if (is_list(col_merge_style))
		assert([for (s = col_merge_style) is_string(s)] == [for (_ = col_merge_style) true],
				"col_merge_style can only contain strings");
}
// use: set the merge style in the merge pairs, default value depends on joining cols
function def_cms(i1, i2) = is_curved && no_keys[i1] == no_keys[i2] ? "smooth" : "at_length";
cm_pairs = is_undef(cm_idx) ? undef :
		   is_undef(col_merge_style) ? [for (c = cm_w) concat(c, def_cms(c[0][0], c[1][0]))] : 
		   is_list(col_merge_style) ? [for (c = cm_w) concat(c, col_merge_style[i])] :
		   [for (c = cm_w) concat(c, col_merge_style)];


/************************** variable processing section **************************/
// this is where we use the validated config vars to prepare the vars for the
// actual calls to solid-generating modules

col_w = 2*col_m + switch_w;
// since cols is empty for flats
FC_NPTS = 101;

// cumulative offset
o = concat([[0,0,0]], cumsum([for (o = offs) o+[0,0,col_w]]));

// column outlines (flat columns was very much an after thought, tacked on way later than it should've)
pts = is_curved ? // first deal with curved cols, then flat ones
		[for (i = [0:len(cols)-1]) stroke2d(cols[i], col_t)] :
		[for (nk = no_keys) stroke2d(line_path2d(fc_len(nk), FC_NPTS), col_t)];
lps = [for (p = pts) len(p)];

// indices of the bottom and top surface points of column
b_idxs = [for (lp = lps) [each [0:(lp/2-1)]]];
t_idxs = [for (lp = lps) [each [(lp/2):(lp-1)]]];
// we need the lowest points of the stroked bezier curve (upper and lower line)
pmi = is_curved ? // again, first curved cols, for flats we just calc the index
		[for (i = [0:len(cols)-1]) minyi(pts[i])] :
		[for (i = [0:len(no_keys)-1]) round(FC_NPTS * fc_key_x(align_keys[i])/fc_len(no_keys[i]))];
t_pmi = is_curved ?
		[for (i = [0:len(cols)-1]) t_idxs[i][minyi([for (ii = t_idxs[i]) pts[i][ii]])]] : 
		[for (i = pmi) 2*FC_NPTS-i-1];

// find the position (edge of prev cap + cap2cap + switch_w/2), angle, and
// pre-emc-merger index (on the top surface) for each switch cutout. note that
// indices on top surface increase "backwards", hence stp = -1 last
co = is_curved ?
		[for (i = [0:len(cols)-1]) let(co_p = pts[i][t_pmi[i]])
			concat(reverse(co_pts(pts[i], [co_p, 0, t_pmi[i]+1],
								  align_keys[i], cap2cap, stp = [-1, 1])),
				   [[co_p, 0, t_pmi[i]-1]],
				   co_pts(pts[i], [co_p, 0, t_pmi[i]-1],
						  no_keys[i]-align_keys[i]-1, cap2cap, stp = [1, -1]))
		] :
		[for (nk = no_keys) let(l = fc_len(nk))
			[for (i = [0:nk-1]) let(x = fc_key_x(i))
				[[x, col_t/2], 0, 2*FC_NPTS-floor(FC_NPTS*x/l)-1]
			]
		];

// merge the cut off the columns and create the final outlines.
pts2 = [for (i = [0:len(pts)-1])
			let(rep = col_end_pt(pts[i], co[i][0], col_rem, pmi[i], [-1, 1]),
				fep = col_end_pt(pts[i], co[i][len(co[i])-1], col_fem, pmi[i], [1, -1]))
				col_with_emc(pts[i],
							 rep, rear_emc(rep[0], rep[1], col_t, -1),
							 fep, front_emc(fep[0], fep[1], col_t, 1))];

/*
tp_idx = 1;
for (i = [0:len(pts2)-1]) echo(str(i), pts2[i][2]);
translate([0, 50]) let(mp = pts2[tp_idx]) {
	for (i = [0:len(mp[0])-1]) le(i%2==0?1:2) translate(mp[0][i]) circle(i%2==0?0.02:0.01);
	color("red") le(3) for (epi = concat([0], mp[2])) translate(mp[0][epi]) circle(0.005);
//	color("yellow") translate(mp[0][0]) circle(0.3);
}
*/

// we're now done with generating the columns, so lets pack them into an array of "column structs"
// as such:
// [[outline_0, width, cutouts_0, btm_index_0, offset_0, angle_0, thickness, emc_i], ... [outline_N, ...]].
columns = [for (i = [0:len(pts2)-1])
			[
				pts2[i][0],					// the outline
				col_w,						// column width, TODO: move to col_keys to make it per-switch
				[for (j = [0:len(co[i])-1])	// cutouts with index adjustments
					[co[i][j][0], co[i][j][1], co[i][j][2]+pts2[i][3][1]]
				],
				pmi[i]+pts2[i][3][0],		// adjusted bottom point index
				o[i],						// offset
				angs[i],					// angle
				is_list(col_t) ? col_t[i] : col_t,
				pts2[i][2]					// emc indices
			]
		  ];
		  
// test the column joinings..
/*c1_i = 1; c2_i = 2; //len(columns)-1;
echo("c1 len:", len(columns[c1_i][0]), "c2 len:", len(columns[c2_i][0]));
join_cols(columns[c1_i], 3, columns[c2_i], 3);*/

// next we do the same for the switches and caps
col_keys = [for (i = [0:len(pts2)-1])
				[is_list(switch_w) ? switch_w[i] : switch_w,
				 is_list(switch_rec) ? switch_rec[i] : switch_rec,
				 is_list(cap_w) ? cap_w[i] : cap_w,
				 is_list(cap_h) ? cap_h[i] : cap_h,
				 is_list(cap_btm_clear) ? cap_btm_clear[i] : cap_btm_clear]
		   ];



/********************************** helpers for variables **********************************/

/* helpers for flat columns */
function fc_len(nk) = col_rem + col_fem + 6*col_t + nk*cap_w + (nk-1)*cap2cap; // TODO: fix the 6*col_t
function fc_key_x(ki) = 3*col_t+col_rem + ki*(cap_w+cap2cap) + cap_w/2;

/* cutout index finder functions (done in 2D) */
// finds the indices of the cutouts on the top surface (called before emcing the column
// outlines)
// args:
//  pts:   list of points
//  co0:   first (i.e. given) cutout, on the form [[co0.x, co0.y], angle_at(co0), ni],
//         where ni is the index of the closest point in pts in the direction of stp[1].
//  count: number of cutout points to find
//  d:     distance between caps
//  stp:   stp[0] is the direction in x the cap should propagate, stp[1] is the index
//         step to take if two caps overlap (basically sign for x and sign and sz for idx)
// returns: [[[co0.x, co0.y], co0_a, co0_i], ..., [[coN.x, coN.y], coN_a, coN_i]] where coX_a
//          is the angle at coX and coX_i the index of the preceding point in pts. note that
//          since "preceeding" point doesn't have to be preceding in index but can be in
//          x-direction (depending on which side of the align key you are on). as of 2022-08-11
//          it doesn't really matter for the rest of the code so i won't bother changing it
//          until someone asks, or it causes some issue.
function co_pts(pts, co0, count, d, stp = [1, 1], _acc = []) =
	(count < 1 ?
		_acc :
		let(n_co = co_pt(pts, co0[0], p_rrot2d([stp[0]*cap_w/2, cap_h], co0[1], co0[0]),
						 [-1*stp[0]*cap_w/2, cap_h], co0[2], d, stp[1])) (
			co_pts(pts, n_co, count-1, d, stp, concat(_acc, [n_co]))
		)
	);
// this one's called a bunch of times, so it takes a bunch of args to avoid calcing the
// same thing over and over again. it finds the position of the switch cutout next to
// the cutout with its base center at p0 and upper cap corner at p0_e. arg cap_e is the
// offset of the cap corner of the switch that goes into the cutout we're looking for,
// other than that the args are explained in co_pts.
function co_pt(pts, p0, p0_e, cap_e, ni, target_dist, istp) =
	assert((-1 < ni) && (ni < len(pts)-1), "ERROR 01: Switch cutout point not found")
	let(a = angle_of(pts[ni-istp], pts[ni]),
		p1_e = p_rrot2d(cap_e, a, pts[ni]),
		d = l2_sigdist(p0, p0_e, p1_e)) (
		d >= target_dist ?
			(d == target_dist ?
				[pts[ni], a, ni+istp] :
				let(prev_p1_e = p_rrot2d(cap_e, a, pts[ni-istp]),
					p = p_on_p0p1(prev_p1_e, p1_e, p0_e, target_dist))
					( [p-p_rot2d(cap_e, a), a, ni] )
			) :
			co_pt(pts, p0, p0_e, cap_e, ni+istp, target_dist, istp)
	);

/* finding the column cut off coords (also 2D) */
// find end coords of column. this is done by first finding the indices pni and ni in
// pts through which the line segments of the last cap in the column (as given by
// cap_p0/1) crosses, and then stepping further to min_dist. since it's recursive the
// caller should calc cap_pt0/1, even though it could be done here using pi and cap_w/h.
// note: the lnseg cap_p0,cap_p1 needs to actually intersect the column, so the caller
//       (likely) needs to extend_p0p1_to_y(...) it to, e.g. pts[pmi].y 
// args: pts, index of point to start at, upper corner of cap, "lower corner" of cap,
//       distance to move, direction in [x, pts_index] to step (last arg is only used
//       for recursion and should not be given).
// returns: end point, angle at end point, and the index of the preceding point in pts
function end_pt(pts, pi, cap_p0, cap_p1, min_dist, stp, i = 1) =
	let(ni = pi+i*stp[1], pni = ni-stp[1]) // next index, prev next index
	assert((0 <= ni) && (ni < len(pts)), "ERROR 03: No end point found!") (
		intersects(cap_p0,cap_p1, pts[pni],pts[ni]) ?
			let(i_pt = intersection_pt(cap_p0,cap_p1, pts[pni],pts[ni]),
				dist = l2_dist(i_pt, pts[ni]),
				ret = dist == 0 ? // if i_pt == pts[ni] just return
					[i_pt, angle_at(pts, ni), pni] : 
					dist >= min_dist ? // if i_pt-pts[ni] dist >= min_dist step further
						concat(_substep(i_pt, pts[ni], (dist-min_dist)*stp[0]), pni) :
						_step2d(pts, ni, min_dist, stp, dist))
			( ret ) :
			end_pt(pts, pi, cap_p0, cap_p1, min_dist, stp, i+1)
	);

// end_pt helper, finds a point pX along path pts that is located at a distance d
// (also along the path) from p0, by moving _away_ (in index direction stp[1]) from pts[i].
function _step2d(pts, pi, d, stp, accum_dist = 0) =
	let(ni = pi+stp[1], to_ni = l2_dist(pts[pi], pts[ni]))
	assert((0 <= ni) && (ni < len(pts)), "ERROR 02: End point distance is too long") (
		accum_dist+to_ni >= d ?
			(accum_dist+to_ni == d ?
				[pts[ni], angle_at(pts, ni), pi] :
				concat(_substep(pts[pi], pts[ni], (d-accum_dist)*stp[0]), pi)
			) :
			_step2d(pts, ni, d, stp, accum_dist+to_ni)
	);

// another end_pt helper
function _substep(p0,p1, d) = let(a = angle_of(p0, p1)) [[p0.x+cos(a)*d, p0.y+sin(a)*d], a];
	
// wrapper for the end_pt-function that uses some globals and such
// args: pts, cut out, margin after last cap, btm pt index, direction (see end_pt)
function col_end_pt(pts, co, m, pmi, stp) =
	let(cap_p0 = p_rrot2d([stp[0]*cap_w/2, cap_h], co[1], co[0]),
		cap_p1 = p_rrot2d([stp[0]*cap_w/2, 0], co[1], co[0]))
		( end_pt(pts, co[2], cap_p0, extend_p0p1_to_y(cap_p0, cap_p1, pts[pmi].y), m, stp) );

/* merge the emc with a column outline (obvs 2D) */
// naïve implementation of finding the intersection between two paths. i feel like there
// sould be a better way tho.. but that's for another day. this pupper just recurses thru
// the possibly intersecting part of the emcs (emc_step_emc) for each possible linesegment
// in pts (emc_step_pts).
// args: pts, end_pt-output used to generate rear emc, rear "edge mask cut" (output of
//       rear_emc), front end_pt-output, and front emc.
// returns: [pts, ep, emc_i, corrs], where pts is the column outline path with "edge-mask
//          applied", i.e. the edges "cut off" and replaced with the emc-paths. ep is an
//          index pair, first one is of the rear end point and the second the front one.
//          emc_i contains three indices, 1st point (bottom) of the front emc, last point
//          in the femc, and 1st point in the rear emc (last point of remc is the last or
//          first point of the outline, depending how you wanna define it). corrs contain
//          bottom and top layer index corrections for the column outline.
function col_with_emc(pts, rep, rem, fep, fem) = 
	let(N = len(pts), ri = N-rep[2]-1, fi = N-fep[2]-1,
		rear = emc_step_pts(rem, pts, ri, (rem[len(rem)-1].x <= pts[ri].x ? -1 : 1)),
		front = emc_step_pts(fem, pts, fi, (fem[len(fem)-1].x >= pts[fi].x ? 1 : -1)),
		new_remish = [for (i = [0:rear[1]]) rem[i]], // top to btm (excl. btm point)
		new_fem = concat([front[0]], [for (i = [front[1]:-1:0]) fem[i]]), // btm to top
		pts = concat([rear[0]], [for (i = [rear[2]:front[2]]) pts[i]],
					 new_fem, [for (i = [fep[2]:rep[2]]) pts[i]],
					 new_remish),
		ep0 = len(pts)-len(new_remish)+minxi(new_remish),
		ep = [rear[0].x > pts[ep0].x ? ep0 : 0, 2+(front[2]-rear[2])+maxxi(new_fem)],
		corrs = [-1*rear[2]+1, 2+(front[2]-rear[2])+(2+front[1])-fep[2]]) (
		[pts, ep,
		 [front[2]+corrs[0]+1, 				// first femc point (btm)
		  front[2]+corrs[0]+len(new_fem),	// last femc point (top)
		  len(pts)-len(new_remish)],		// first remc point (top)
		 corrs]
	);
		
// finds the intersection of the emc and the btm part of the column. note that it doesn't use
// the intersects function since openscad doesn't do lazy eval (so using it would be slower).
// it also doesn't do any error-checking.. it prolly should tho.
// args: emc, pts, starting index in pts, direction to step in pts.
// returns: the intersection point and preceding index in emc and pts on the
//          format [[pt.x, pt.y], emc_i, pts_i]. note that the "preceding pts index" is defined
//          as the index of the end of the intersecting lnseg in pts that is closest to len(pts)/4
// TODO: error checking
function emc_step_pts(emc, pts, i, d) =
	let(res = emc_step_emc(pts[i], pts[i+d], emc, len(emc)-1)) (
		res ? concat(res, closer(i, i+d, len(pts)/4)) : emc_step_pts(emc, pts, i+d, d)
	);
// step thru an emc until we hit an intersection or lower emc-point is above upper pts-point.
// args: pts[i], pts[i+d], emc, emc index to start at
// returns: intersection point and preceding emc index as [[pt.x, pt.y], emc_i] or false if no
//          linesegment in the emc intersects the p0p1 linesegment
function emc_step_emc(p0, p1, emc, i) = i < 1 ? false : // first check if we're outta bounds
	(sign(xprod(p0, p1, emc[i])) != sign(xprod(p0, p1, emc[i-1])) ?            // Q1) can they intersect?
		(sign(xprod(emc[i], emc[i-1], p0)) != sign(xprod(emc[i], emc[i-1], p1)) ? // A1a) yes, Q2) do they?
			[intersection_pt(p0,p1, emc[i],emc[i-1]), i-1] 						: // A2a) yes they do
			(emc[i].y > p1.y ? false : emc_step_emc(p0, p1, emc, i-1))) :		  // A2b) no they don't
		(emc[i].y > p1.y ? false : emc_step_emc(p0, p1, emc, i-1))			      // A1b) no they can't
	);



// now that everythiing is generated we call the module
module render_columns() { columns(columns, cm_pairs, col_keys, base_clear); }
module render_thumbcluster() { thumbcluster(pts, o); }
module render_walls() { walls(pts, o); }
module render_base() { assert(false, "BASE IS NOT NOT IMPLEMENTED YET"); }
module render_thumbcluster_base() { assert(false, "THUMB CLUSTER BASE IS NOT NOT IMPLEMENTED YET"); }

/************************** the modules that generate the solids **************************/
/* implementation modules (and some support funcs nd such) */
module walls(pts, o) {
	assert(false, "walls don't work yet..");
/*
	// these two are mainly for brevity, just rotates all the pts in the list of lists 90 degs
	// around the x-axis - I DON'T KNOW WHY THIS DOESN'T WORK!?
//	function rot90x(list) = [for (pts = list) [for (p = pts) p_xyrot3d(p, [90])]];
	function rot90x(pts) = [for (p = pts) p_xyrot3d(p, [90])];
	maxy = 40;
	difference() {
		linear_extrude(height = maxy)
			projection_outline(wall_t) columns(pts, o, do_cutout = false);
		translate([0,0, -1*miny(o)+0.001]) for (i = [0:(len(pts)-1)]) {
			// columns
			pm = pts[i][pmi[i]];
			// since for the diff we need the limiting 3D-surface of the columns "extruded", we
			// have to take into account the tilt of the columns, i.e. include both bottom and
			// top lines of the shape_path_extend-stroke
			if (angs[i][0] == 0) {
				// DON'T KNOW WHY THE LIST-BASED rot90x DOESN't WORK!
				function_grapher([rot90x(col_side(pts[i], b_idxs[i], pm, -col_w/2, angs[i], o[i])),
								  rot90x(col_side(pts[i], b_idxs[i], pm, col_w/2, angs[i], o[i]))],
								 -maxy);
			} else if (angs[i][0] > 0) {
				function_grapher([rot90x(col_side(pts[i], b_idxs[i], pm, -col_w/2, angs[i], o[i])),
								  rot90x(col_side(pts[i], b_idxs[i], pm, col_w/2, angs[i], o[i])),
								  rot90x(col_side(pts[i], t_idxs[i], pm, col_w/2, angs[i], o[i]))],
								 -maxy);
			} else {
				function_grapher([rot90x(col_side(pts[i], t_idxs[i], pm, -col_w/2, angs[i], o[i])),
								  rot90x(col_side(pts[i], b_idxs[i], pm, -col_w/2, angs[i], o[i])),
								  rot90x(col_side(pts[i], b_idxs[i], pm, col_w/2, angs[i], o[i]))],
								 -maxy);
			}
			// column joiners..
			if (i > 0) {
				p_1 = floor(pmi/2);
				p_4 = pmi + ceil((lps[i]/2-pmi)*2/3);
				pomi = minyi(pts[i-1]);
				pom = pts[i-1][pomi];
				po_1 = floor(pomi/2);
				po_4 = pmi + ceil((lps[i]/2-pmi)*2/3);
				function_grapher([rot90x(col_side(pts[i-1], [po_1:po_4], pom, -col_w/2, angs[i-1],
												  o[i-1] + p_xyrot3d([0, 0, col_w], angs[i-1]))),
								  rot90x(col_side(pts[i], [p_1:p_4], pm, -col_w/2, angs[i], o[i]))],
								  -maxy);
			}
		}
	}
*/
}


// the actual thumbcluster
module thumbcluster(pts, o) {
	// polyhedron points for a "slice" of the side of a key (used to merge keys)
	function side_ph_pts(k, w, d, h, m, side) = // key, switch_w, switch_w, col_t, tc_m, tcm[0][1]
		let(bp = rect_path2d([0,0], [w/2+m, d/2+m], open = false), // "base plate"
			shft = (side % 2 > 0 ? [0,m,0] : [m,0,0]) * (side < 2 ? -1 : 1))
			[for (i = [side, side+1], dp = [[0,0,0], shft],	z = [k[0].z-h/2, k[0].z+h/2])
				k[0]+p_ordered_rot3d([bp[i].x, bp[i].y, z]+dp, k[1], len(k) < 3 ? [0,1,2] : k[2])];
	// comments below describe the faces of a "slice" on the left side of a key-cube
	ph_faces = [[0,2,3,1], // back
				[2,0,4,6], // bottom
				[4,5,7,6], // front
				[1,3,7,5], // top
				[0,1,5,4], // left (remember left side slices!)
				[3,2,6,7]]; // right
	// we need the col point from where we start
	ci = floor(tc_col_i);
	p = let(rep = col_end_pt(pts[ci], co[ci][0], col_rem, pmi[ci], [-1, 1]),
			pp = pt_trans(rep[0], pts[ci][pmi[ci]], (tc_col_i-ci-0.5)*col_w, angs[ci], o[ci]))
			p_xrot3d(pp, 90) - [0,0, miny(o)+base_clear+col_t/2];
	// and now we make the holders
	translate(p+tc_o) // 
		ordered_rot(is_num(tc_a[0]) ? tc_a : tc_a[0],
					is_num(tc_a[1]) ? [0,1,2] : tc_a[1])
			difference() {
				union() {
					for (tcp = tc) { // the keyholders
						translate(tcp[0]) {
							ordered_rot(tcp[1], (len(tcp) < 3 ? [0,1,2] : tcp[2])) {
								
								// TODO make proper key holders based on proper cutouts
								
								cube([switch_w+2*tc_m, switch_w+2*tc_m, col_t], center = true);
								if (prev_ghost_caps) {
									%translate([0, 0, cap_h/2 + cap_btm_clear]) {
										difference() {
											cube([cap_w, cap_w, cap_h], center = true);
											translate([0,0,-1])
												cube([cap_w-2, cap_w-2, cap_h], center = true);
										}
									}
								}
							}
						}
					}
					// the merging!
					for (tcm = tc_merge) hull() {
						polyhedron(side_ph_pts(tc[tcm[0][0]], switch_w, switch_w, col_t, tc_m, tcm[0][1]),
								   ph_faces);
						polyhedron(side_ph_pts(tc[tcm[1][0]], switch_w, switch_w, col_t, tc_m, tcm[1][1]),
								   ph_faces);
					}
				}
				for (tcp = tc)
					translate(tcp[0])
						ordered_rot(tcp[1], (len(tcp) < 3 ? [0,1,2] : tcp[2]))
							rotate([0,0,90]) cube([switch_w, switch_w, col_t+2], center = true);
			}
}


