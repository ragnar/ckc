// columnar keyboard configurator (CKC), curved column example config
// Started out very similar to dactyl & co, but with individually configable columns.
// The reason for its existance is that i suck at clojure and figured that iT
// prObaBlY WoUldN't bE tOO TrICkY tO jUsT wRiTE iT mYSeLf. Then everything went way
// outta hand and now most things are configgable.
//

/*********************** NOTE: all values are given in mm ***********************/  

/* general configuration */
// NOTE: while most of these are values you measure or personal preference it is
// important that you make sure your col_t, switch_rec, and cap_btm_clear values
// work together. have an extra look at all heavily tilted and curved sections in
// the preview window.
col_t = 4;			// thickness of the columns.
col_m = 3;			// column margin, the width of the material on the sides of the switches
col_rem = 1;		// column rear end margin, the width behind the first switch
col_fem = 1;		// column front end margin, the width in front of the last switch
col_merge = true;	// merge columns or not. to not merge either just delete it or set to false.
					// to merge all columns set it to true. to merge only selected column pairs
					// set it to [[mp0_i1, mp0_i2], [mp1_i1, mp0_i2], ..., [mpN_i1, mpN_i2]],
					// where mpX_i1/2 stands for merge pair index 1 and 2. index 1 is the index
					// of the column closest to the thumb.
					
base_clear = 0;		// clearence of lowest point of all columns from base

switch_w = 15.6; 	// width of the switch you're using
switch_rec = 0;		// the depth to which the switches are recessed into the columns. a value
					// of zero here means the lateral center of the "upper" part of the switch
					// sits on top of the column (switches sitting on parts of the columns with
					// heavy curvature will still have their edges recessed a bit due to the
					// curvature).
cap_w = 17.7;		// width of caps
cap_h = 4;			// height of caps (when not pressed), measured from the top surface of the
					// column
cap_btm_clear = 1;	// distance from the top surface of an (imaginary) flat column to the
					// bottom of a cap when it is fully pressed down.
cap2cap = 2.5;		// in-column cap-to-cap distance.
wall_t = 4;			// wall thickness

/*
// TODO: move these to some advanced config section. and to start with we should only have
// two styles, "regular" and "split". also worth noting that any and all lists have to match

// in case you want to merge your columns and have column pairs that overlap (anywhere, it's
// enough if they just barely touch at some point) you will need to set the col_merge_style
// of that pair to "custom". the reason why custom is not the default is that it can produce
// column mergers that doesn't look very nice. in case it still doesn't work please consult
// the docs [link]. column mergeing can get a bit messy and for more advanced layouts you'll
// likely have to make custom merging solids using the styling options of "custom".
col_merge_w = col_m;			// single value or [[rmw1, lmw2], [rmw2, lmw3], ..., [rmwN-1, lmwN]]
								// where rmwX and lmwX are right and left side merge width for column
								// X, respectively, and N the number of columns
col_merge_style = "at_length";	// "smoot" or "at_length", or a list of them, one for each pair
*/

/* column configuration */
// column paths, first points should always be [0, 0]. Note that the columns are given
// from your thumbs and outward, i.e. left to right for your right hand and right to
// left for your left.
//
// We start with a bezier point-based example. For people tracing the movement of their
// finger tips using something like a drawing pad, this is likely to be the best way to
// define their column outlines (bezier-fitting of tracings is also a planned feature
// for the GUI-version of CKC).
b_pts = [ // bezier control points
			[[0, 0], [20, -38], [80, -20], [120, -4]],	// 1st column, lets call it extra pointy
			[[0, 0], [20, -38], [80, -18], [120, -4]],	// 2nd column, pointy
			[[0, 0], [20, -38], [80, -18], [120, 3]],	// 3rd column, longy
			[[0, 0], [20, -38], [80, -18], [120, -4]],	// 4th column, ringy
			[[0, 0], [20, -30], [80, -15], [100, -4]]	// 5th column, pinky
		];
b_stp_sz = 0.01; // bezier step size
cols = [for (i = [0:len(b_pts)-1]) bezier_curve(b_stp_sz, b_pts[i])]; // generate the column paths
/*
// Here's an example using arcs of ellipses, which might be easier if you're tracing
// finger movement using pen and paper (or just prefer it that way).
// As noted above each column has to start in [0,0] and as it so happens, calling
// rebase_path2d with no additional arguments translates a path to start in origo.
// so to make the column-generating code a bit less verbose we add a function that
// just generates an arc starting in origo (arc_path2d's first arguments are the
// radii of an ellipse - or just a single raius for a circle - and an angle span):
arc_at_origo = function(r, a) rebase_path2d(arc_path2d(r, a, 100));
// then we generate our columns by giving a radii-pair (since the columns should
// propagate in x the "x-radius" is always the major one) and an angle span
cols = [
			arc_at_origo([60, 35], [-135, -30]),
			arc_at_origo([60, 35], [-135, -30]),
			arc_at_origo([70, 30], [-120, -10]),
			arc_at_origo([70, 55], [-135, -30]),
			arc_at_origo([60, 50], [-135, -30])
	   ];
*/

// relative column offsets, note that there are N-1 sets of values as they're all
// relative to the previous column. values are given in triplets where the order is
// front/back (pos/neg, also known as column stagger), up/down (pos/neg), spacing
// (only pos).
// note 1: the spacing refers to the distance between the columns at their lowest
// point for curved columns and the alignment key for flat (see align_keys below).
// note 2: the width of a column is 2*col_m + switch_w, which means that the minimal
// distance, i.e. when the third value is set to 0, between two switch cutouts is
// 2*col_m.
// ex: if first set of values are [-8, -6, 4] then the lowest point of the 2nd
//	   column is located 8 mm behind (closer to your palm) the 1st one, its base is
//     6 mm below the 1st's base, and they are - again at the lowest point -
//     separated by 4 mm.
offs = [
			[  0,  -8,   1.5], 	// extra pointy to pointy 
			[  6,  -6,   4],	// pointy-longy
			[ -2,   2,   4],	// longy-ringy
			[ -4,   4,   4]		// ringy-pinky
	   ];
// angles of columns in degrees. first value is the tilt, second the rotation, so
// if you look along your lower arm towards your hand, the first value would be the
// angle you have to tilt your head to the right (positive value) or left (negative)
// to look along the top of the column. the second how much you would have to rotate
// your head. note that the rotation is given around the lowest point of the column.
angs = [
			[ 25,   3],	// extra pointy
			[ 10,   3],	// pointy
			[  0,   0],	// longy
			[  0,  -2],	// ringy
			[ -5,  -3]	// pinky
	   ];
// shape of short end of columns. check helpers.scad for optional arguments for the
// individual functions, emc stands for "edge mask cut"
//function rear_emc(p,a,w,d) = emc_arc(p,a,w,d);		// arced short ends.
function rear_emc(p,a,w,d) = emc_flat(p,a,w,d);	// flat short ends.
//function front_emc(p,a,w,d) = emc_arc(p,a,w,d);		// arced short ends.
function front_emc(p,a,w,d) = emc_flat(p,a,w,d);	// flat short ends.

/* keys configuration (done by column) */
// number of keys in each column (index corresponds to column)
no_keys = [4, 4, 4, 4, 4];
// (zero-based) index of bottom key, 0 is closest to the palm
align_keys = [1, 1, 1, 1, 1];

/* thumb cluster configuration */
tc_col_i = 0.9;	// "index" of column at which's rear end the cluster will "start
				// at". the fraction indicates where on the column it should start.
tc_m = col_m;			// thumb cluster key margin (used by helper functions, you
						// can ignore it if you're defining your own cluster from
						// scratch).
tc_a = [-5, 0, -40];	// angles of rotation given in order [lean, tilt, rotation],
						// in this example the whole cluster is rotated 5 degrees
						// counter clockwise, tilted 0 degrees "towards the user",
						// and leaning 40 degrees "downward".
tc_o = [-10, 0, -10];	// offset (applied after rotation) for the whole cluster

// so, time for the actual keys then! tc should contain the list of keys in the
// thumbcluster, format is [[x, y, z], [a_lean, a_tilt, a_rotation]], where x, y,
// and z are relative to the tc_col_i point, a_lean is the rotation around the x
// axis, a_tilt around the y axis, and a_rotation around z axis. because i wasn't
// thinking properly when implementing this all keys in the tc should be rotated
// 90 degrees around the z-axis before any other rotation is applied. sorry. there
// are a few helper functions (see below) that will generate common thumbclusters
// for you. by default the roations are performed in the order they are given.
// should you want any other order a third element in the format above can be
// added: [[x, y, z], [a_lean, a_tilt, a_rotation], [i_1, i_2, i_3]], where
// i_1/2/3 are the zero based indices of the angles that should be rotated
// 1st/2nd/3rd. for example a third element of [2,1,0] will apply the rotations in
// reverse order.
// IMPORTANT NOTE: the first key must be the one closest to the tc_col_i point,
// the next one should be the closest to the first, and so on. if the keys are
// not given in this order the render is likely to get messed up.

/************************ HELPER 1: single row cluster. ************************/
/* here's an example call that generates a three key cluster                   */
//tc = tc_row(3);
// mandatory arguments for tc_row:
// n	- number of keys in the row
// optional args:
// cd	- cap distance, def val: cap2cap
// sw	- switch width, def val: switch_w
// m	- margin, def val: tc_m
/*******************************************************************************/

/************************ HELPER 2: multi row cluster. *************************/
/* example below generates two rows with three keys in the first and two in    */
/* the second. second row is also shifted half a key such that the two keys    */
/* are centered under the first.                                               */
//tc = tc_rows([3, 2], [0, (switch_w+tc_m)/2]);
// mandatory arguments for tc_rows:
// n 	- vector where each element gives the number of keys in the row with the
//		  same index as that element
// optioal arguments:
// ro	- row offsets, vector where each element gives an 1D-offset (applied
//		  before rotation) to the row with the same index as that element
// cd	- cap distance, def val: cap2cap
// cw	- cap width, def val: switch_w
// m	- margin, def val: tc_m
/*******************************************************************************/

/***************** HELPER 3: distribute keys along a 2D-curve. *****************/
/* The example below uses two helpers to generate the path, arc_path2d which   */
/* makes a path with 80 points spanning the arc from 45 degrees to 135 degrees */
/* of an ellipse who's major axis is 2*50 mm and minor 2*30 mm. the path is    */
/* then "rebased" (couldn't think of a better name for it) such that the first */
/* point is located at [0,0] (default value of third arg to rebase_path2d) and */
/* second goes in the direction [0,1] (along the positive y-axis). finally     */
/* n = 4, i.e. four keys are distributed along the path.                       */
//tc = tc_path2d(rebase_path2d(arc_path2d([50, 30], [45, 135], 80), [0,1]), 4);
// mandatory arguments for tc_path2d:
// pts	- the 2d path
// n	- number of keys to distribute along pts
// optional args:
// cd	- cap distance, def val: cap2cap
// cw	- cap width, def val: cap_w
// m	- margin, def val: tc_m
/*******************************************************************************/

/************** HELPER 4: like helper 3 but with an initial tilt. **************/
/* so you can easily do an "inward clicking" thumbcluster.                     */
/* TODO: more docs here                                                        */
tc = tc_path25d(rebase_path2d(arc_path2d([50, 30], [-45, -135], 80), [0,1]), -75, 3);
/************** HELPER 4: like helper 3 but with an initial tilt. **************/

// merge sides of keys in thumbcluster. keys and sides are given in pairs as such:
// [[i_0, s_0], [i_1, s_1]], where i_0/1 are the indices of the keys to merge and
// s_0/1 the sides of the keys indexed in the order top, left, bottom, right. An
// example: tc_merge = [[[0, 1], [1, 3]], [[1, 1], [2, 3]]]; will merge the left
// side of the first key with the right side of the second, and the left side of
// second with the right side of the third. Another example:
// tc_merge = [[for (i = [0:len(tc)-2]) [[i, 1], [i+1, 3]]]; would merge all left
// and right sides from the 4 key cluster generated in the tc_path2d-example above. 
// from the tc_path2d-example above
// NOTE: as row clusters generated with the helpers are already merged there is
//       no need to merge them, i.e. tc_merge can be left empty (tc_merge = []).
tc_merge = [for (i = [0:len(tc)-2]) [[i, 1], [i+1, 3]]];

/* wall configuration */
// TODO
// wa_angs = [0, 0]; // [angle out from columns, angle in towards base], both
//					 // are 0-90.. i think.
// wa_alns = [1, 1]; // [from col, towards base].
// wa_style = "kinked" || "smooth"; // not sure about this one

/* compilation configuration */
prev_ghost_caps = true; // show cubic, full range "ghost caps" in preview window,
						// this does not affect the stl in any way (just might be
						// useful when iterating your design)
// uncomment the part(s) you want to render. to render the left hand side uncomment
// the mirror call (and closing curly bracket).
//mirror([0,1,0]) {
render_columns();
render_thumbcluster();
//render_walls();
//render_base();
//render_thumbcluster_base();
//}

/********************* END OF KEYBOARD CONFIGURATION SECTION ********************/

// these needs to be here
use <utils.scad>;
include <helpers.scad>;
use <ckc.scad>;
