// columnar keyboard configurator (CKC), flat column example config
// for more descriptive comments see the curved example config
//

/*********************** NOTE: all values are given in mm ***********************/  

/* general configuration */
// NOTE: while most of these are values you measure or personal preference it is
// important that you make sure your col_t, switch_rec, and cap_btm_clear values
// work together. have an extra look at all heavily tilted and curved sections in
// the preview window.
col_t = 4;			// thickness of the columns.
col_m = 3;			// column margin, the width of the material on the sides of the switches
col_rem = 2;		// column rear end margin, the width behind the first (closest to palm) switch
col_fem = 2;		// column front end margin, the width in front of the last switch
base_clear = 0;		// clearence of lowest point of all columns from base
switch_w = 15.6; 	// width of the switch you're using
switch_rec = 0;		// the depth to which the switches are recessed into the columns. a value
					// of zero here means the lateral center of the "upper" part of the switch
					// sits on top of the column (switches sitting on parts of the columns with
					// heavy curvature will still have their edges recessed a bit due to the
					// curvature).
cap_w = 17.7;		// width of caps
cap_h = 4;			// height of caps (when not pressed), measured from the top surface of the
					// column
cap_btm_clear = 1;	// distance from the top surface of an (imaginary) flat column to the
					// bottom of a cap when it is fully pressed down.
cap2cap = 2.5;		// in-column cap-to-cap distance.
wall_t = 4;			// wall thickness

/* column configuration */
// note that for a flat column the cols var is simply removed as the columns are defined just 
// by the number of keys in them:
no_keys = [1, 4, 4, 4, 4, 4, 4]; // seven columns, first one has one key, the rest has four
// indices of the aligning keys (the ones you'll give offs and angs for) for each column,
// 0 is closest to the palm
align_keys = [0, 1, 1, 1, 1, 1, 1];

// relative column offsets, note that there are N-1 sets of values as they're all
// relative to the previous column. values are given in triplets where the order is
// front/back (pos/neg, also known as column stagger), up/down (pos/neg), spacing.
// since we want a tented keyboard and fc_tenting returns both our offs and angs,
// we store the initial offset-config in a temp variable:
my_offsets  = [
			[ 20,  0,  0],	// 2nd col alignment key offset from 1st col alignment key
			[  3,  0,  0],	// 3rd to 2nd
			[  2,  0,  0],	// 4th to 3rd
			[ -3,  0,  0],	// and so on..
			[ -5,  0,  0],
			[ -1,  0,  0]
	   ];
/*
// we then call fc_tenting with the offsets..
tmp = fc_tenting(no_keys, offs = my_offsets, angle = 25);
// and store the output in offs and angs:
offs = tmp[0];
angs = tmp[1];
/*/
offs = my_offsets;
angs = [for (_ = [1:len(no_keys)]) [0,0]];


// shape of short end of columns. check utils.scad for optional arguments for the
// individual functions, emc stands for "edge mask cut"
//function rear_emc(p,a,w,d) = emc_arc(p,a,w,d);	// arced short ends.
function rear_emc(p,a,w,d) = emc_flat(p,a,w,d);		// flat short ends.
//function front_emc(p,a,w,d) = emc_arc(p,a,w,d);	// arced short ends.
function front_emc(p,a,w,d) = emc_flat(p,a,w,d);	// flat short ends.

/* thumb cluster configuration */
tc_col_i = 5.2;	// "index" of column at which's rear end the cluster will "start".
				// the fraction indicates where on the column it should start.
tc_m = col_m;	// thumb cluster margin
tc_a = [angs[0][0]/2, 0, -10];	// angles of rotation given in order
								// [lean, tilt, rotation]. so here we lean (tent)
								// the thumbluster half as much as the columns,
								// don't tilt it at all, but rotate it 10 degrees
								// towards the palm.
tc_o = [-5, 0, 0];	// offset (applied after rotation) for the whole cluster. here
					// we move it 5 mm towards the palm.

// so, time for the actual keys then! tc contains a list of keys in the thumbcluster,
// format is [[x, y, z], [a_lean, a_tilt, a_rotation]], where x, y, and z are
// relative to the tc_col_i point, a_lean is the rotation around the x axis, a_tilt
// around the y axis, and a_rotation around z axis. because i wasn't thinking
// properly when implementing this all keys in the tc should be rotated 90 degrees
// around the z-axis before any other rotation is applied. sorry. there are a few
// helper functions (see below) that will generate common thumbclusters for you.
// by default the roations are performed in the order they are given. should you
// want any other order a third element in the format above can be added:
// [[x, y, z], [a_lean, a_tilt, a_rotation], [i_1, i_2, i_3]], where i_1/2/3 are
// the zero based indices of the angles that should be rotated 1st/2nd/3rd. for
// example a third element of [2,1,0] will apply the rotations in reverse order.
// IMPORTANT NOTE: the first key must be the one closest to the tc_col_i point,
// the next one should be the closest to the first, and so on. if the keys are
// not given in this order the render will get messed up.

// see curved example for the other thumbcluster helper-functions

// HELPER 3: distribute keys along a 2D-curve. The example below uses two helpers
// to generate the path, arc_path2d which makes a path with 80 points spanning the
// arc from 45 degrees to 135 degrees of an ellipse who's major axis is 2*60 mm
// and minor 2*15 mm. the path is then "rebased" (couldn't think of a better name
// for it) such that the first point is located at [0,0] (default value of third
// arg to rebase_path2d) and second goes in the direction [0,1] (along the
// positive y-axis). finally n = 5, i.e. five keys are distributed along the path.
tc = tc_path2d(rebase_path2d(arc_path2d([60, 15], [45, 180], 90), [0,1]), 5);

// merge all keys in the thumbcluster
tc_merge = [for (i = [0:len(tc)-2]) [[i, 1], [i+1, 3]]];

/* wall configuration */
// TODO
// wa_angs = [0, 0]; // [angle out from columns, angle in towards base], both
//					 // are 0-90.. i think.
// wa_alns = [1, 1]; // [from col, towards base].
// wa_style = "kinked" || "smooth"; // not sure about this one

/* compilation configuration */
prev_ghost_caps = true; // show cubic, full range "ghost caps" in preview window,
						// this does not affect the stl in any way (just might be
						// useful when iterating your design)
// uncomment the part(s) you want to render. to render the left hand side uncomment
// the mirror call (and closing curly bracket).
//mirror([0,1,0]) {
render_columns();
render_thumbcluster();
//render_walls();
//render_base();
//render_thumbcluster_base();
//}

/********************* END OF KEYBOARD CONFIGURATION SECTION ********************/

// these needs to be here
use <utils.scad>;
include <helpers.scad>;
use <ckc.scad>;