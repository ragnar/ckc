# THIS DOCUMENT IS OUTDATED

Do not rely on this info at all, i'm just keeping it around as it will be the basis for future docs.

# Config

As the main goal of lpcats is configurability there's alot of config options. The easiest way, i think, to describe these options is to describe how the keyboard is constructed in openSCAD, i.e. how they are used in the code. That way i hope you'll get an understanding not just of what they do, but also of how they affect each other, and maybe it'll make modding the spagetti-monster that is the code a bit easier. If that's not what you're looking for but rather just a concise description of the different config options we'll start with that.

## Config options

| Name | Description | Type | Figure |
| ---- | ----------- | ------ | ------ |
|`col_t`| thickness of the columns. | float > 0 | 2 |
|`col_m`| column margin, the width of the material on the sides of the switches switches. | float &ge; 0 | 4 |
|`col_rem`| column rear end margin, the width behind the first switch. | float > 0 | 3 |
|`col_fem`| column front end margin, the width in front of the last switch. | float > 0 | 3 |
|`base_clear`| clearence of lowest point of all columns from base. | float ≥ 0 | - |
|`switch_w`| width of the switch you're using. | float > 0 | 4 |
|`switch_rec`| the depth to which the switches are recessed into the columns. a value of zero here means the lateral center of the "upper" part of the switch sits on top of the column (switches sitting on parts of the columns with heavy curvature will still have their edges recessed a bit due to the curvature). | float ≥ 0 | - |
|`cap_w`| width of caps. | float > 0 | 3 |
|`cap_h`| height of caps (when not pressed), measured from the top surface of the column. | float > 0 | - |
|`cap_btm_clear`| distance from the top surface of an (imaginary) flat column to the bottom of a cap when it is fully pressed down. | float > 0 | - |
|`cap2cap`| in-column cap-to-cap distance. | float > 0 | 3 |
|`wall_t`| wall thickness. | float > 0 | - |
|`cols`|  column bezier points, first one should always be [0, 0]. The bezier step size is set in the compilation configuration section. note that these are given from your thumbs and outward, i.e. left to right for your right hand and right to left for your left. | NxM  points | 1 |
|`offs`| relative column offsets, note that there are N-1 sets of values as they're all relative to the previous column. values are given in triplets where the order is front/back (pos/neg, also known as column stagger), up/down (pos/neg), spacing (only pos). | (N-1)x3 floats | - |
|`angs`|  angles of columns in degrees. first value is the tilt, second the rotation, so if you look along your lower arm towards your hand, the first value would be the angle you have to tilt your head to the right (positive value) or left (negative) to look along the top of the column. the second how much you would have to rotate your head. note that the rotation is given around the lowest point of the column. | Nx2 floats | 4 |
|`rear_emc`|  shape of rear end of columns. | func | 3 |
|`front_emc`|  shape of front end of columns. | func | 3 |
|`no_keys`| number of keys in each column (index corresponds to column). | Nx1 ints > 0 | 3 |
|`align_keys`|  (zero-based) index of bottom key, 0 is closest to the palm. | Nx1 ints ≥ 0 | 3 |
|`b_stp_sz`| bezier step size | float > 0 | - |
|`prev_ghost_caps`|  show cubic, full range "ghost caps" in preview window, this does not affect the stl in any way (just might be useful when iterating your design) | bool | - |


| Const/Type  | Value                                       |
| ----- | ------------------------------------------- |
| N     | Number of columns                           |
| M     | Number of bezier points                     |
| point | Two element vector given on the form [x, y] |
| func  | Function                                    |

## Figure colors

In the figures i've tried to be consistent in my use of colors, here's a short description of what they mean.

| Color     | Meaning                                                      |
| --------- | ------------------------------------------------------------ |
| Yellow    | Config options that control the shape of different parts of the keyboard. |
| Green     | Config options that are simple lengths.                      |
| White     | Only used for text and arrows indicating what the text relates to, almost all config option names are given in white text. |
| Aqua-blue | Points, lines, and solids that are used when constructing the keyboard |
| Magenta   | Points/lines that are of interest but not *necessarily* used for constructing the keyboard |
| Lavender  | Supporting points, lines, and text that are not used in construction. |
| Grey      | Lines connecting points that are related, not used in construction. |
| Red       | Areas and volumes that removed from solids in construction.  |

## OpenSCAD construction

This will be the "long form" description of the config variables. Some of the text from the table above will be repeated.

### 2D Columns

So, it all starts with the bezier points, see fig 1, if you're unfamiliar with bezier curve just check out the [wiki page](https://en.wikipedia.org/wiki/B%C3%A9zier_curve), it covers the theory and has some really nice illustrations. In fig. 1, you can see the bezier points for column `i`, together with the name of the config option in lpcats.scad, `cols`. In the actual code the curves outlining the columns are generated using the control points in `cols` together with the stepsize `b_stp_sz`. Note that the latter is the same for all columns.

![Bezier points](fig01.png)
When the outlines have been generated they are stroked with a width of `col_t`. After the stroke the bottom point, highlighted in fig. 2, of the top surface of what will become the column is located.
![Stroked bezier curve](fig02.png)
It is from this bottom point that the (virtual) keycaps are distributed. The number of keys on a column `i` is given by `no_keys[i]` and how they are distributed and determined by which (zero based) index is given by `align_keys[i]`. In fig. 3 `align_keys[i] = 1` and `no_keys[i] = 4`, which means the key with index 1 is centered on the bottom point, one key is placed "behind" it (to the left of it in fig. 3) and two "in front" of it. The top of each `cap_w` (virtual, rectangular) wide cap is separated by a distance  `cap2cap`. Behind the first cap, at a distance of `col_rem` (rem meaning *rear edge margin*), the column is then masked out and cut using the function  `rear_emc` (emc = *edge mask cut*). The same is done in front using distance `col_fem` and cut function `front_emc`. The cut functions can at the moment take two different values, `emc_arc`  (as `rear_emc` in fig. 3) or `emc_flat` (`front_emc` in fig. 3). With the former the [$fn](https://en.wikibooks.org/wiki/OpenSCAD_User_Manual/The_OpenSCAD_Language#$fa,_$fs_and_$fn) value can be controlled, and the latter the angle of the flat cut.
![2D column with measurements](fig03.png)

### 3D Columns

When all have been distributed, masked, and cut the column is extruded into a 3D-shape...
TODO: continue.
![3D column with rotation and tilt angles](fig04.png)



### Thumbcluster

TODO...
go thru the helpers, and the helpers' helpers (*_path2d)