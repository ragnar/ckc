// this pupper is just for illustration purposes - code is not pretty! it has
// cut and pasted sections, uses even more globals, and so on. also note that
// it getting rid of the dotSCAD dependence here is probably at the bottom of
// prio list
use <dotSCAD/bezier_smooth.scad>;
use <../utils.scad>;
use <../ckc.scad>;

/* configuration */
b_pts = [[0, 0], [20, -38], [80, -22], [120, -4]];
b_stp = 0.01; //0.2;
bc_pts = bezier_curve(b_stp, b_pts);
c_fg = "cyan"; 				// foreground ("main") color
c_sup = "lavenderblush";	// supporting color
c_bg = "dimgray";			// background lines
c_hl = "magenta";			// highlight color
c_ln = "springgreen";		// color indicating lengths
c_shape = "yellow"; 		// color indicating shapes
c_txt = "white";			// text and arrows
$fn = 80;

/* this is my "config" here */
col_t = 4;
cap_w = 17.7;
cap_h = 8;
btm_keys = 1;
no_keys = 4;
cap_btm_clear = 0.5;
col_rem = 3;
col_fem = 4;
cap2cap = 3;

switch_rec = 0;
switch_w = 15.6; 
col_m = 3;
col_w = 2*col_m + switch_w;

/* some generic functions and modules */
function p_avg2d(pts, _a = [0,0], _i = 0) = let(lp = len(pts))
	(_i < lp ? p_avg2d(pts, _a+pts[_i], _i+1) : [_a.x/lp, _a.y/lp]);

function cyl(a, r) = [cos(a)*r, sin(a)*r];

module dashed_line2d(pts, dash_ln = 2, dash_w = 0.15, dash_space = 2, clr = c_bg) {
	assert(len(pts) > 1, "You need at least two points dummy");
	l = dash_ln+dash_space;
	for (i = [0:len(pts)-2]) {
		nf = l2_dist(pts[i], pts[i+1])/l;
		f = nf - floor(nf);
		n = floor(nf) + (f > dash_ln ? 1 : 0);
		dd = (f*l + (n < nf ? dash_space : -dash_ln) + dash_ln)/2;
		color(clr)
			let(a = angle_of(pts[i], pts[i+1]), ca = cos(a), sa = sin(a))
				for (ii = [0:n-1])
					translate(pts[i]+[ca*(ii*l+dd), sa*(ii*l+dd)])
						rotate(a)
							square([dash_ln, dash_w], center = true);
	}
}

module hull_line(pts, r) {
	for (i = [0:len(pts)-2]) hull() {
		translate(pts[i]) circle(r);
		translate(pts[i+1]) circle(r);
	}
}

module x(w = 1.5, h = 1.5, t = 0.3) {
	l = sqrt(w^2+h^2);
	rotate([0,0,45]) square([l, t], center = true);
	rotate([0,0,-45]) square([l, t], center = true);
}

module ln_ind(l, t = 0.3, tip_w = -1) {
	w = tip_w > 0 ? tip_w : l/8;
	translate([-l/2+t/2, 0]) square([t, w], center = true);
	square([l, t], center = true);
	translate([l/2-t/2, 0]) square([t, w], center = true);
}
module ln_p0p1(p0, p1, t = 0.3, tip_w = -1) {
	w = tip_w > 0 ? tip_w : l2_dist(p0,p1)/8;
	a = angle_of(p0, p1);
	d = 0.2;
	function dr(r) = [cos(a)*r/2, sin(a)*r/2];
	hull() {
		translate(p0+dr(d)) rotate([0,0,a]) square([d, t], center = true);
		translate(p1-dr(d)) rotate([0,0,a]) square([d, t], center = true);
	}
	translate(p0+dr(t)) rotate([0,0,a]) square([t, w], center = true);
	translate(p1-dr(t)) rotate([0,0,a]) square([t, w], center = true);
}

module arrow(from = [0, 0], to = [10, 0], t = 0.2, arm_ln = -1, a_from = undef, a_to = undef) {
	a = atan2(from.y-to.y, from.x-to.x);
	at = (is_undef(a_to) ? a : a_to);
	aln = (arm_ln > 0 ? arm_ln : l2_dist(from, to)/8);
	if (is_undef(a_from) && is_undef(a_to)) {
		hull_line([from, to], t/2);
	} else let(af = (is_undef(a_from) ? a : a_from), dl = l2_dist(from, to)/6) {
		hull_line(bezier_smooth([from, from-cyl(af, dl), to+cyl(at, dl), to], dl),
				  t/2);
	}
	hull_line([to+cyl(at-45, aln), to, to+cyl(at+45, aln)], t/2);
}

module le(h = 1) {
	linear_extrude(height = h) children();
}

/* modules and functions used to generate the figures in the docs */
module f01_bez(b_pts, bc_pts, r_ep = 1, r_cp = 1, r_bc = 0.2) {
	translate(b_pts[0]) {
		color(c_shape) circle(r_ep);
		color(c_txt) translate([1.6,0])
			text("cols[i][0]", size = 2, halign = "left", valign = "center");
	}
	translate(b_pts[len(b_pts)-1]) {
		color(c_shape) circle(r_ep);
		color(c_txt) translate([-1.6,0.2])
			text("cols[i][4]", size = 2, halign = "right", valign = "bottom");
	}
	for (i = [1:len(b_pts)-2]) translate(b_pts[i]) {
		color(c_shape) circle(r_cp);
		color(c_txt) translate([0,-1.8])
			text(str("cols[i][", i, "]"), size = 2, halign = "center", valign = "top");
	}
	dashed_line2d(b_pts);
	color(c_sup) for (p = bc_pts) translate(p) circle(r_bc);
}

// TODO ADD cap_h here too!
module _f02_03(bc_pts, r, f) {
	function col_end_pt(pts, co, m, pmi, stp) =
		let(cap_p0 = p_rrot2d(co[0], [stp[0]*cap_w/2, cap_h], co[1]),
			cap_p1 = p_rrot2d(co[0], [stp[0]*cap_w/2, 0], co[1]))
			( end_pt(pts, co[2], cap_p0, extend_p0p1_to_y(cap_p0, cap_p1, pts[pmi].y), m, stp) );
	
	pts = stroke2d(bc_pts, col_t);
	color(c_fg) for (p = pts) translate(p) circle(r);
	lp = len(pts);
	t_idxs = [for (i = [(lp/2):(lp-1)]) i];
	t_pmi = t_idxs[minyi([for (i = t_idxs) pts[i]])];
	color(c_hl) translate(pts[t_pmi]) circle(1.5*r);
	
	if (f == 2) {
		// draw the bez curve pts to ref prev figure
		color(c_sup) for (p = bc_pts) translate(p) circle(r);
		// length indicator for col_t
		let(a = angle_at(pts, 0), p_txt = bc_pts[2]+[5, 0]) {
			color(c_ln) le()
				translate(cyl(a, -1))
					ln_p0p1(pts[0], pts[lp-1], tip_w = 1);
			// arrow and text for col_t ln ind
			color(c_txt) le() {
				arrow(p_txt, bc_pts[0]-cyl(a, 0.3), a_from = 0, a_to = a);
				translate(p_txt+[1,0])
					text("col_t", size = 2, halign = "left", valign = "center");
			}
		}
	} else {
		pmi = minyi(pts);
		// calc the cap positions
		co0 = [pts[t_pmi], 0, t_pmi-1];
		co = concat(reverse(co_pts(pts, [co0[0], co0[1], t_pmi+1], btm_keys, cap2cap, stp = [-1, 1])),
					[co0],
					co_pts(pts, [co0[0], co0[1], t_pmi-1], no_keys-btm_keys-1, cap2cap, stp = [1, -1]));
		// draw the caps and add btm_keys indicator
		for(i = [0:len(co)-1]) {
			translate(co[i][0]) {
				rotate([0, 0, co[i][1]])
					translate([0, cap_h/2]) {
						#square([cap_w, cap_h], center = true);
						color(i != btm_keys && i < len(co)-1 ? c_sup : c_shape) le()
							text((i != btm_keys ?
									(i < len(co)-1 ? str(i) : "no_keys[i]-1") :
									"align_keys[i]"),
								 size = 2, halign = "center", valign = "center");
					}
				}
			}
		
		// cap_w indicator
		translate(pts[t_pmi]-[0,0.5]) {
			color(c_ln) ln_ind(cap_w, tip_w = 1);
			color(c_txt) translate([0, -1])
				text("cap_w", size = 2, halign = "center", valign = "top");
		}
			
		// cap2cap indicators
		for(i = [0:len(co)-2])
			let(p0 = p_rrot2d(co[i][0], [cap_w/2, cap_h], co[i][1]),
				p1 = p_rrot2d(co[i+1][0], [-cap_w/2, cap_h], co[i+1][1]),
				a = angle_of(p0, p1)+90,
				p_txt = [pts[pmi].x, pts[t_pmi].y+cap_h*2]) {
			color(c_ln) le() ln_p0p1(p0, p1, tip_w = 1);
			color(c_txt) le() {
				arrow(p_txt, p_avg2d([p0, p1])+cyl(a, 1), arm_ln = 1.5, a_to = a);
				translate(p_txt + [0, 1])
					text("cap2cap", size = 2, halign = "center", valign = "bottom");
			}
		}
		// calc and draw the rear mask
		rep = col_end_pt(pts, co[0], col_rem, pmi, [-1, 1]);
		remc = emc_arc(rep[0], rep[1], col_t, -1);
		#polygon(concat(remc, edge_mask(pts, [for (i = [rep[2]:(lp-2)]) i],
										[pts[0], pts[lp-1], -1], col_t)));
		// highlight rear mask cut
		color(c_shape) le() hull_line(remc, 0.15);
		// add arrow and text to rear mask cut
		color(c_txt) le() let(ci = floor(len(remc)/3),
							   a = angle_at(remc, ci)+90,
							   p_txt = [remc[0].x+1, remc[0].y+5]) {
			arrow(p_txt, remc[ci]+cyl(a, 0.5), a_to = a);
			translate(p_txt + [0.3, 1])
				text("rear_emc", size = 2, halign = "center", valign = "bottom");
		}
		
		// same for front mask
		fep = col_end_pt(pts, co[len(co)-1], col_fem, pmi, [1, -1]);
		femc = emc_flat(fep[0], fep[1], col_t, 1);
		#polygon(concat(femc, edge_mask(pts, [for (i = [fep[2]:-1:(lp/2+1)]) i],
										[pts[lp/2-1], pts[lp/2], 1], col_t)));
		color(c_shape) hull_line(femc, 0.15);
		// add arrow and text to rear mask cut
		color(c_txt) le() let(a = angle_of(femc[0], femc[1])+90,
							   p_txt = [femc[0].x+1, femc[0].y+4]) {
			arrow(p_txt, p_avg2d(femc)+cyl(a, 0.5), a_to = a);
			translate(p_txt + [-0.3, 1])
				text("front_emc", size = 2, halign = "center", valign = "bottom");
		}
		// col_rem indicator with arrow and text
		let(p0 = rep[0],
			p1 = p_rrot2d(co[0][0], [-cap_w/2, 0], co[0][1]),
			a = angle_of(p0, p1)-90) {
			color(c_ln) le() ln_p0p1(p0, p1, tip_w = 1);
			color(c_txt) le() let(p_to = p_avg2d([p0, p1])+cyl(a, 1),
								   p_txt = p_to+cyl(a, col_t+1)) {
				arrow(p_txt, p_to);
				translate(p_txt + [-0.3, -1])
					text("col_rem", size = 2, halign = "center", valign = "top");
			}
		}
		// and finally col_fem
		let(p0 = p_rrot2d(co[len(co)-1][0], [cap_w/2, 0], co[len(co)-1][1]),
			p1 = fep[0],
			a = angle_of(p0, p1)-90) {
			color(c_ln) le() ln_p0p1(p0, p1, tip_w = 1);
			color(c_txt) le() let(p_to = p_avg2d([p0, p1])+cyl(a, 1),
								   p_txt = p_to+cyl(a, col_t+1)) {
				arrow(p_txt, p_to);
				translate(p_txt + [0, -1])
					text("col_fem", size = 2, halign = "center", valign = "top");
			}
		}
	}
}

module f02_stroked(bc_pts, r = 0.2) { _f02_03(bc_pts, r, 2); }

module f03_mishmash(bc_pts, r = 0.2) { _f02_03(bc_pts, r, 3); }

module f04_3d_angs(bc_pts) {
	function col_end_pt(pts, co, m, pmi, stp) =
		let(cap_p0 = p_rrot2d(co[0], [stp[0]*cap_w/2, cap_h], co[1]),
			cap_p1 = p_rrot2d(co[0], [stp[0]*cap_w/2, 0], co[1]))
			( end_pt(pts, co[2], cap_p0, extend_p0p1_to_y(cap_p0, cap_p1, pts[pmi].y), m, stp) );
	
	pts = stroke2d(bc_pts, col_t);
	lp = len(pts);
	t_idxs = [for (i = [(lp/2):(lp-1)]) i];
	t_pmi = t_idxs[minyi([for (i = t_idxs) pts[i]])];
	pmi = minyi(pts);
	// calc the cap positions
	co0 = [pts[t_pmi], 0, t_pmi-1];
	co = concat(reverse(co_pts(pts, [co0[0], co0[1], t_pmi+1], btm_keys, cap2cap, stp = [-1, 1])),
				[co0],
				co_pts(pts, [co0[0], co0[1], t_pmi-1], no_keys-btm_keys-1, cap2cap, stp = [1, -1]));
	// rear mask
	rep = col_end_pt(pts, co[0], col_rem, pmi, [-1, 1]);
	rem = concat(emc_arc(rep[0], rep[1], col_t, -1),
				 edge_mask(pts, [for (i = [rep[2]:(lp-2)]) i],
						   [pts[0], pts[lp-1], -1], col_t));
	// front mask
	fep = col_end_pt(pts, co[len(co)-1], col_fem, pmi, [1, -1]);
	fem = concat(emc_flat(fep[0], fep[1], col_t, 1),
				 edge_mask(pts, [for (i = [fep[2]:-1:(lp/2+1)]) i],
						   [pts[lp/2-1], pts[lp/2], 1], col_t));
	translate([0,0,-pts[pmi].y]) rotate([90]) {
		difference() {
			linear_extrude(height = col_w) difference() {
				polygon(pts);
				polygon(rem);
				polygon(fem);
			}
			for(i = [0:len(co)-1])
				translate([co[i][0].x, co[i][0].y, col_w/2])
					rotate([0, 0, co[i][1]]) {
						translate([0, -switch_rec])
							cube([switch_w, 3*col_t, switch_w], center = true);
/*						%translate([0, cap_h/2 + cap_btm_clear]) difference() {
							cube([cap_w, cap_h, cap_w], center = true);
							translate([0,-1]) cube([cap_w-2, cap_h, cap_w-2], center = true);
						} */
					}
		}
		txt_rot = [0, -90, -35];
		// switch_w ind
		let(p = co[len(co)-1][0])
			translate([p.x, p.y, col_m+switch_w/2])
				rotate(txt_rot) {
					color(c_ln) ln_ind(switch_w);
					color(c_txt) translate([0, -1]) le(0.4)
						text("switch_w", size = 2, halign = "center", valign = "top");
				}
		// col_m ind
		let(p = co[len(co)-2][0])
			translate([p.x-2, p.y+1, col_m*1.5+switch_w+0.1])
				rotate(txt_rot) {
					color(c_ln) ln_ind(col_m, tip_w = 1);
					color(c_txt) translate([col_m/2+1, 0]) le(0.4)
						text("col_m", size = 2, halign = "left", valign = "center");
				}
		// center 3d-arrow
		color(c_hl, 0.3)
			translate([pts[t_pmi].x, pts[t_pmi].y-col_t, col_w/2])
				rotate([-90]) {
			ch = 30+col_t;
			cylinder(h = ch, r = 0.3);
			translate([0,0,ch]) cylinder(ch/10, 0.5, 0.01);
		}
		// rotation arrow
		color(c_shape)
			translate([pts[t_pmi].x, pts[t_pmi].y+cap_h*2, col_w/2])
				rotate([-90, -90])
					le() let(a_end = 345, t = 0.3, r = col_w/2, p_end = cyl(a_end, r)) {
			hull_line(arc_path2d(r, [205, a_end]), t);
			hull_line([p_end+cyl(a_end-45, 3), p_end, p_end-cyl(a_end+45, 3)], t);
		}
		// rotation text
		color(c_txt)
			translate([pts[t_pmi].x-col_w/2, pts[t_pmi].y+cap_h*2+1.5, col_w/2])
				rotate(txt_rot)
					le(0.4)
						text("angs[i][1]", size = 2, halign = "center", valign = "bottom");
		// tilt arrow
		color(c_shape)
			translate([pts[t_pmi].x, pts[t_pmi].y+30-col_w/4, col_w/2])
				rotate([-90, -90, 90])
					le() let(a_end = 165, t = 0.3, r = col_w/2, p_end = cyl(a_end, r)) {
			hull_line(arc_path2d(r, [25, a_end]), t);
			hull_line([p_end+cyl(a_end-45, 3), p_end, p_end-cyl(a_end+45, 3)], t);
		}
		// rotation text
		color(c_txt)
			translate([pts[t_pmi].x, pts[t_pmi].y+30.5+col_w/4, col_w/2])
				rotate(txt_rot)
					le(0.4)
						text("angs[i][0]", size = 2, halign = "center", valign = "bottom");
	}
}

/* this i do manually */
translate([0, 3*60])
f01_bez(b_pts, bc_pts);
// txt01: some short intro, link to bezier curve @ wiki

translate([0, 2*60])
f02_stroked(bc_pts);
// f02: stroked bezier with lowest point highlighted in red and arrows showing off <col_t>
// txt02: col_t, thickness of the column

translate([0, 1*60])
f03_mishmash(bc_pts);
// f03: f02 + ghost_caps + cap2cap indicators + col_r/fem indicators + edge masks
// txt03: the red dot is the lowest point, which is where the key with index <btm_keys>, will
//		  sit, i.e. in f03 the <btm_keys>-index for the depicted column is 1. The distance
//		  between the top edges of the caps is given by <cap2cap>, seen in f03 in COLOR, and
//		  distance from the bottom of the leftmost (rear) cap to the start of the mask is
//		  <col_rem> (column rear margin). The corresponding distance on the right hand side
//		  (front) is <col_fem>. Also shown in f03 is the <rear_edge_mask_cut> and
//		  <front_edge_mask_cut>, the former set to <emc_arc> and the latter to <emc_flat>, as
//		  depicted the masks are applied in 2D before extruding them into 3D-solids.

f04_3d_angs(bc_pts);
// f04: 3D column with normal centered at miny and arrows indicating rot and tilt
// txt04: after extrusion they are rotated and tilted, angles given in <angs>.

// f05: 3 3D columns showing offset and no_keys
