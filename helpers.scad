// CKC helpers
// this is basically just utils that aren't generic enough to end up in utils.scad
use <utils.scad>;

/************************** column related stuff **************************/
// translate and rotate a column side.. basically what is happening here is, in 
// order:
// translate a line (pts[idx]) so its lowest point (pm) is in origo, embed it in
// 3d (z), rotate it (a), and then translate to an offset (o).
function pt_trans(p, pm, z, a, o) = p_xyrot3d([p.x-pm.x, p.y-pm.y, z], a) + o;
function col_side_explicit(pts, pm, z, a, o) = [for (p = pts) pt_trans(p, pm, z, a, o)];
function col_side(c, z_sign = 1) = col_side_explicit(c[0], c[0][c[3]], z_sign*c[1]/2, c[5], c[4]);
// TODO: col_side with per-switch width (z)

// functions that generate the shape of the short edge of a column, the "cut".
// these should all take the arguments cut point (p), angle of column at p (a), thickness of
// column (w), and direction of cut in x (given as -1 or 1, d). following these, a function
// may take any number of optional arguments (that have to have default values).
// emc_arc opt args:
// np_arc	- number of points in the arc
// ln_arn	- length of the arc
function emc_arc(p, a, w, d, np_arc = 30, ln_arc = 0) =
	let(r = (ln_arc <= 0 ? [w, w] : [w, ln_arc])) (d < 0 ?
		[for (ap = arc_path2d(r, 90, np_arc)) [p.x-w, p.y]+p_rot2d(ap, 90+a, [w, 0])] :
		[for (ap = reverse(arc_path2d(reverse(r), 90, np_arc))) [p.x, p.y-w]+p_rot2d(ap, a, [0, w])]
	);

// emc_flat opt args:
// a_cut - the angle (relative to the column) of the cut.
function emc_flat(p, a, w, d, a_cut = 45) =
	[p, p+p_rot2d([d*sqrt((w/cos(a_cut))^2-w^2), -w], a)];


/************************** flat columns specific things **************************/
// this one, in it's current form, should work just fine with curved columns as well. i think.
function fc_tenting(no_keys, offs = [], angs = [], angle = -1, height = -1) =
	assert(angle > 0 || height > 0, "ERROR FCT01: To tent you have to give either angle or height")
	let(nk = len(no_keys),
		o = len(offs) > 0 ? concat(offs, [[0,0,0]]) : [for (i = [0:nk-1]) [0,0,0]],
		col_w = 2*col_m + switch_w,
		a = angle > 0 ? angle : atan(height/(nk*col_w+sum_col(o, 2))),
		ca = cos(a), sa = sin(a),
		kp = [for (x = 0, y = 0, i = 0; i < nk; x = x+col_w+o[i][2], y = y-o[i][1], i = i+1)
				[x-(x*ca-y*sa), x*sa+y*ca]]) ( // [dx, y]
		[[for (i = [0:nk-2]) [o[i][0], kp[i][1]-kp[i+1][1], kp[i][0]-kp[i+1][0]]],
		 len(angs) > 0 ? [for (i = [0:nk-1]) angs[i]+[a,0]] :
						 [for (i = [0:nk-1]) [a,0]]
		]
	);


/************************** thumb cluster helpers **************************/
// single row
function tc_row(n, cd = cap2cap, cw = cap_w, m = tc_m) = let(io = m+cw/2)
	[ for (i = [0:n-1]) [[-io, io+i*(cw+cd), 0], [0,0,0]] ];

// n-row
function tc_rows(n, ro = [], cd = cap2cap, cw = cap_w, m = tc_m) = let(io = m+cw/2) [
		for (i = [0:len(n)-1]) let(yo = (len(ro) > i ? ro[i] : 0))
			for (ii = [0:n[i]-1])
				[[-io-i*(cw+cd), io+yo+ii*(cw+cd), 0], [0,0,0]]
	];

// along path:
// user facing function, assigns first point in path to pts[0] and then just calls the
// internal funcs for the next n-1 ones.
function tc_path2d(pts, n, cd = cap2cap, cw = cap_w, m = tc_m, _acc = []) =
	len(_acc) < 1 ? tc_path2d(pts,n-1,cd,cw,m, [[pts[0], angle2_of(pts[0], pts[1]), 1]]) :
		(n < 1 ?
			let(a0 = angle2_of(pts[0], pts[1]), io = (m+cw/2))
				[for (p = _acc) [[p[0].x-io, p[0].y+io, 0], [0,0,p[1]-a0]]] :
			let(pp = _acc[len(_acc)-1])
				tc_path2d(pts,n-1,cd,cw,m, concat(_acc, [_tcp2d_next(pts, pp, pp[2], cd, cw)])));

// steps thru the indices in pts to find the first possible point, this is basically just
// to avoid too many rects_overlap and rect2rect_dist calls.
function _tcp2d_next(pts, pp, ni, cd, cw) =
	assert(ni < len(pts), "ERROR: tc path too short (or too few pts) to fit all keys") (
		l2_dist(pp[0], pts[ni]) >= cw ?
			_tcp2d_step2tcdist(pts, pp, ni, cd, cw) :
			_tcp2d_next(pts, pp, ni+1, cd, cw)
	);

// steps forward along pts as long as cap distance is too small and then in between points
// to find pos of cap with cd distance to the previous one.
function _tcp2d_step2tcdist(pts, pp, ni, cd, cw) = 
	assert(ni < len(pts), "ERROR: tc path too short (or too few pts) to fit all keys")
	let(na = angle2_of(pts[ni-1], pts[ni])) (
		rects_overlap([pp[0], [cw, cw]/2, pp[1]], [pts[ni], [cw, cw]/2, na]) ?
			_tcp2d_step2tcdist(pts, pp, ni+1, cd, cw) : // openscad doesn't do lazy eval
			let(pr = rect_path2d(pp[0], [cw, cw]/2, pp[1]),
				nr = rect_path2d(pts[ni], [cw, cw]/2, na),
				d = rect2rect_dist(pr, nr)) (
				d < cd ? _tcp2d_step2tcdist(pts, pp, ni+1, cd, cw) :
					let(pd = rect2rect_dist(pr,
											rect_path2d(pts[ni-1], [cw, cw]/2,
														angle2_of(pts[ni-2], pts[ni-1]))),
						dr = l2_dist(pts[ni-1], pts[ni])*(cd-pd)/(d-pd)) // approximate linearity
						[pts[ni-1]+dr*[cos(na), sin(na)], na, ni]
			)
	);

// along 2.5D path is really just adding a to the rotation of each key =)
function tc_path25d(pts, a, n, cd = cap2cap, cw = cap_w, m = tc_m) = 
	[for (k = tc_path2d(pts,n,cd,cw,n)) [k[0], k[1]+[0,a,0], [1,0,2]]];
