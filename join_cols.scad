// CKC column joiners
// since it turns out that what style of column joining works and looks best is highly dependent
// on the relative angle and stagger of the columns i decided to place these moodules in a
// separate file, that way it's easier to extend later on. the only "user facing" function is
// join_cols, the rest should have a _jc_-prefix and only be called using the style-parameter in
// join_cols (the value set in col_merge_style).

include <utils.scad>;
include <helpers.scad>;

module join_cols(c1, mw1, c2, mw2, style) {
	style_split = splitstr(style);
	s = len(style_split) == 0 ? style : style_split[0];
	if (s == "smooth") {
		_jc_smooth(c1, mw1, c2, mw2);
	} else if (s == "at_length") {
		_jc_at_length(c1, mw1, c2, mw2);
	} else if (s == "custom") {
		let (lss = len(split_style))
			_jc_custom(c1, mw1, c2, mw2, lss > 0 ? select(split_syle, [1:lss-1]) : []);
	} else assert(false, str(s, " is not a known column merge style"));
}

module _jc_smooth(c1, mw1, c2, mw2) {
	// TODO:
	// 0mw with a seam in the middle:
	// 1) check angle to determine if tops or btms are closer to each other
	// 2) lookuptable-average the seam in between the closer edges + emc
	// 3) make a bottom edge that starts emc btm, then hops to col_t below first top point. it will require another set of faces (since all the ones we have now will be split in two) and it won't solve all problems, but it will look better.
	_jc_custom(c1, mw1, c2, mw2, []);
}

module _jc_at_length(c1, mw1, c2, mw2) {
	// TODO: a bunch of stuff
	// 0) we gotta know emc of cols.. so add that to the cols monster
	// 1) check endpts of both
	// 2) use endpts and emcs from (1) to make new outlines
	// 3) merge.. with smooth maybe?
	_jc_custom(c1, mw1, c2, mw2, []);
}

module _jc_custom(c1, mw1, c2, mw2, style) {
// make two column joining polyhedra that should handle high relative angles and offset
// args:
// c1  - first column: [outline, width, cutouts, btm_index, offset, angle, thickness, emc_i]
// mw1 - width to merge, i.e. overlap of the joining polyhedron and the column
// ...
// style - array of words describing the style of one or both joining polyhedra as such:
//		   1) []
//		   2) [<ph0 c1 closer>, <ph0 c2 closer>, ..., <phN c1 closer>, <phN c2 closer>]
//		   where <... closer> is "neither|both|top|bottom". the words indicate which of the top
//		   and the bottom sections of the side of polyhedron start at the edge of column.

	//
	// TODO:
	// fix the damn crashes when rendering to STL. seems to be order of indices in faces. but
	// who knows..
	//
	assert(len(style) % 2 == 0, "wrong number of words in _jc_custom style argument");
	s = len(style) == 0 ? ["both", "neither", "neither", "both"] : style;
	eps = 0.01;
	
	// the outline a column can be divided into 4 section:
	// 1) bottom, bounded by 0 and btm femc (c[7][0])
	// 2) front emc, btm femc (c[7][0]) and top femc (c[7][1])
	// 3) top, top femc and top remc (c[7][2])
	// 4) rear emc, top remc and 0
	// since style can specify the top section to be at the marging edge of the column and
	// bottom at merge width into the column (with the word "top") or the other way around we
	// use two func to generate the sides of each merging polyhedron. one for the "leaning" ones
	// one for "flat" ones. to make it easier we start with two funcs that transform a section.
	// in practice the second, grad_trans, will only ever transform emc sections of leaning ones.
	// args: column, section indices, z coord to shift to
	function const_trans(c, idxs, z) = [for (i = idxs) pt_trans(c[0][i], c[0][c[3]], z, c[5], c[4])];
	// args: column, section indices (explicit list, no spans!), [z start, z distance to span]
	function grad_trans(c, idx, z) = let(ls = cumlen(c[0], idx), lp = ls[len(ls)-1])
		[for (i = [0:(len(idx)-1)])
			pt_trans(c[0][idx[i]], c[0][c[3]], z[0]+z[1]*ls[i]/lp, c[5], c[4])];
	// cumulative l2_dist of points indexed by idx, needed since emc points are not necessarily
	// equidistant.
	// TODO: avoid double norm (somehow use a let() in there)
	function cumlen(pts, idx) = [0, for (d = 0, i = 1; i < len(idx);
										 d = d+l2_dist(pts[idx[i-1]], pts[idx[i]]), i = i + 1)
											d+l2_dist(pts[idx[i-1]], pts[idx[i]])];
	
	// now that we can transform the sections we make two functions that make the "leaning"
	// and flat sides. to have a more uniform interface (and since making a flat side is just
	// calling col_side) we'll just call the latter ph_side and if it's supposed to lean we
	// call the former.
	// reminder: the columns are centered when extruded so we have to take into account the
	//			 (half) width of them
	// args: column, column side of the ph (left or right) indicated by the sign of the z
	//		 direction, which part of the ouline should be in front (i.e. closer to the gap)
	//		 of the other (top or btm), merge width
	// note: storing the remc is needed to have the points ordered in the same way as in c
	function ph_grad_side(c, z_sign, front, mw) =
		let(t = (strncmp(front, "top", 3) ? 1 : -1), w = c[1]/2-eps,
			bt_z = z_sign*(t == 1 ? [w-mw, w] : [w, w-mw]), // [btm_z, top_z]
			z = [bt_z[0], [bt_z[0], mw*z_sign*t], bt_z[1]],
			remc = grad_trans(c, [each [c[7][2] : (len(c[0])-1)], 0], [bt_z[1], -mw*z_sign*t]))
			concat([remc[len(remc)-1]], const_trans(c, [1 : (c[7][0]-1)], z[0]), // btm
				   grad_trans(c, [each [c[7][0] : c[7][1]]], z[1]),  			 // femc
				   const_trans(c, [(c[7][1]+1) : (c[7][2]-1)], z[2]), 			 // top
				   [for (i = [0:(len(remc)-2)]) remc[i]]);						 // remc
	/*   z       btm,   femc,         top,   remc
	left, top:  [w-mw,  [w-mw,  mw],  w,     [w,     -mw]]
	left, btm:  [w,     [w,     -mw], w-mw,  [w-mw,  mw ]]
	right, top: [-w+mw, [-w+mw, -mw], -w,    [-w,    mw ]]
	right, btm: [-w,    [-w,    mw],  -w+mw, [-w+mw, -mw]]
	*/
	// args: as above
	function ph_side(c, z_sign, front, mw) =
		strncmp(front, "top", 3) || strncmp(front, "bottom", 6) ?
			ph_grad_side(c, z_sign, front, mw) :
			let(z = z_sign*(c[1]/2-eps-(strncmp(front, "neither", 6) ? mw : 0)))
				col_side_explicit(c[0], c[0][c[3]], z, c[5], c[4]);
	
	// now lets generate some polyhedra points! this is done by simply concatenating two sides
	// generated by ph_side.
	pts = [for (i = [0:2:len(s)-1])
			concat(ph_side(c1, 1, s[i], mw1), ph_side(c2, -1, s[i+1], mw2))];
	
	// alrighty, now it's time for the faces. since version 2014.03 you can just feed a bunch
	// of indices to openscad and it will make faces out of them as long as they're in clockwise
	// order. However, when i gave it a "very curved" surface, e.g. one bounded by two emc_arcs,
	// openscad 2022.12.XX produced some funky faces. so, here we'll make the triangular emc faces
	// ourselves! we assume left and right are given in clockwise order, such that we can just
	// return them concatenated if combining them is too tricky.
	function emc_faces(l, r) = let(ll = len(l), lr = len(r), d = ll - lr)
		d == 0 ? concat(triangles(l, r, 0, ll-2), triangles(r, l, 0, lr-2)) :
		// more pts in the left one => left vert wall triangles slope down to the right
		d == 1 ? concat([[l[0], l[1], l[2], r[lr-1]]], // lr = ll-1
						triangles(l, r, 2, ll-2, d), // last RHS i: 1 = (ll-2)+a-(ll-2) => a => 1
						triangles(r, l, 0, lr-2, 2)) : // first LHS i: ll-1 = (lr-2)+a => a => 2
		// more pts in the right one => left vert wall triangles slope up to the right
		d == -1 ? concat(triangles(l, r, 0, ll-2), // last RHS i: 0 = ((ll-2)+a)-(ll-2) => a = 0
						 triangles(r, l, 0, lr-4, -d), // first LHS i: ll-2 = ((lr-4)+a)-0 => a = 1 
						 [[r[lr-3], r[lr-2], r[lr-1], l[0]]]) :
		[concat(l, r)]; // for length diffs > 1 we let openscad deal with it (for now)
	// TODO: make a more generic implementation of emc_faces.
	// since we assume clockwise order of the indices v2 index is e-i + an adjustment
	function triangles(v1, v2, s, e, a = 0) = [for (i = [s:e]) [v1[i], v1[i+1], v2[(e+a)-i]]];
	
	// the rest of the faces aren't very cumbersome, we already know the eight corners so all we
	// have to do is to make sure we list the indices that enclose the six sides in clockwise
	// order. since the faces will be the same for all polyhedra no func is needed.
	ri = len(c1[0]); // right side index
	fcs = concat([
			// left face: just left (first) col in order
			[each [0 : (len(c1[0])-1)]],
			// top face: left top reversed (top remc : top femc) and then
			//           right top in order (top femc : top remc)
			[each [c1[7][2] : -1 : c1[7][1]], each [(ri+c2[7][1]) : (ri+c2[7][2])]],
			// right face: right column reversed
			[each [(len(pts[0])-1) : -1 : ri]],
			// bottom face: left btm reversed (btm femc : 0) and then
			//				right btm in order (0 : btm femc)
			[each [c1[7][0] : -1 : 0], each [ri : (ri+c2[7][0])]]
		],
		// front face: left femc reversed (top femc : btm femc) and then
		//			   right femc in order (btm femc : top femc)
		emc_faces([each [c1[7][1] : -1 : c1[7][0]]],
				  [each [(ri+c2[7][0]) : (ri+c2[7][1])]]),
		// back face: left remc reversed (0, len(c1):top remc) and then
		//			  right remc in order (top remc:len(c2), 0)
		emc_faces([0, each [(len(c1[0])-1) : -1 : c1[7][2]]],
				  [each [(ri+c2[7][2]) : (ri+len(c2[0])-1)], ri])
	);
	
	// and finally do the thing.
	for (ph = pts) polyhedron(ph, fcs, 10);
/*
	// DEEEBUG!!
//	polyhedron(pts[0], fcs, 10);
	function end(a) = a[len(a)-1];
	function qq(a,b,i) = i > 0 ? i < len(a[7]) ? (a[7][i]-a[7][i-1])-(b[7][i]-b[7][i-1]) :
												 (len(a[0])-end(a[7]))-(len(b[0])-end(b[7])) :
								a[7][i]-b[7][i];
	echo("diffs: ", [for (i = [0:len(c1[7])]) qq(c1, c2, i)]);	
	color("green") for (i = fcs[1]) translate(pts1[i]) sphere(0.03);
	color("blue") for (i = fcs[3]) translate(pts1[i]) sphere(0.03);
	color("red") for (i = [0, each [(len(c1[0])-1) : -1 : c1[7][2]],
						   each [(ri+c2[7][2]) : (ri+len(c2[0])-1)], ri])
					translate(pts1[i]) sphere(0.03);
	color("magenta") for (i = fcs[5]) translate(pts1[i]) sphere(0.03);
*/
}


