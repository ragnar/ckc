// CKC solids
// this is where we make (half) a keyboard!

include <utils.scad>;
include <join_cols.scad>;
include <helpers.scad>;


module columns(cols, merge_pairs, keys, base_clear, do_cutout = true) {
// args:
// cols        - array of column [outline, width, cutouts, btm_index, offset, angle, thickness, emc_i]
// merge_pairs - array of pairs [[left_col_idx, left_merge_w], [right_col_idx, right_merge_w], merge_style]
// keys        - array of key [switch_w, switch_rec, cap_w, cap_h, cap_btm_clear]
// base_clear  - scalar, base clearance

	// translate all columns (after rotation so z is now "up" for the columns as well) so the lowest
	// point of them all is base_clear above z = 0.
	translate([0,0, base_clear-min([for(c = cols) c[4].y])]) rotate([90]) difference() {
		union() { // union of all the columns
			for (i = [0:len(cols)-1]) let(outline = cols[i][0],
										  w       = cols[i][1],
										  co      = cols[i][2],
										  btm_pt  = outline[cols[i][3]],
										  o       = cols[i][4],
										  a       = cols[i][5]) {
				// RTL: put lowest point in origo, rotate column to given angle, translate to offset
				translate(o) rotate(a) translate([-btm_pt.x, -btm_pt.y, -w/2]) {
					difference() {
						linear_extrude(height = w) polygon(outline);
						if (do_cutout) finger_column_cutout(w, co, keys[i]);
					}
				}
			}
//			translate([0,30])
			for (mp = merge_pairs)
				join_cols(cols[mp[0][0]], mp[0][1], cols[mp[1][0]], mp[1][1], mp[2]);
		}
		// remove the cap bottoms from the cutouts
		if (do_cutout) {
			for (i = [0:len(cols)-1])
				let(outline = cols[i][0], btm_pt = outline[cols[i][3]])
					translate(cols[i][4])
						rotate(cols[i][5])
							translate([-btm_pt.x, -btm_pt.y, -cols[i][1]/2])
								caps(cols[i][1], cols[i][2], keys[i]);
		}
	}
}

module finger_column_cutout(col_w, co, key, prev_ghost_caps = true) {
// args:
// col_w - column width scalar
// co    - cutout array (see ckc.scad)
// key:  - [switch_w, switch_rec, cap_w, cap_h, cap_btm_clear]
	for (c = co) {
		translate([c[0].x, c[0].y, col_w/2]) {
			rotate([0, 0, c[1]]) {
				translate([0, -key[1]]) switch_cutout();
				if (prev_ghost_caps) {
					%translate([0, key[3]/2 + key[4]]) difference() {
						cube([key[2], key[3], key[2]], center = true);
						translate([0,-1]) cube([key[2]-2, key[3], key[2]-2], center = true);
					}
				}
			}
		}
	}
}

module caps(col_w, co, key) {
// args:
// co  - cutout array (see ckc.scad)
// key - [switch_w, switch_rec, cap_w, cap_h, cap_btm_clear]
//
// TODO: print margin (+0.5 on the width) should be configgable..
	for(c = co)
		translate([c[0].x, c[0].y, col_w/2])
			rotate([0, 0, c[1]])
				translate([0, key[3]/2 + key[4]])
					cube([key[2]+0.5, key[3], key[2]+0.5], center = true);
}

module switch_cutout() {
// TODO: replace with real stuff.. this is just a placeholder
// this is assumed to have its center in the center of finger column
	cube([15.6, 3*4, 15.6], center = true);
}


