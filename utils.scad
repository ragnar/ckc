// CKC utils.
// mostly fairly generic stuff - the rule of thumb is that if a function (or module)
// takes an optional arg that should have a default value set to one of the config
// variables it should go in helpers.scad, not here.
// note: there's not a lot of input validation going on here, this is not intended 
// be a standalone library. CKC is jst a hobby project but even as such it needs
// (quite) a few pretty generic funcs.
//

/************************** random shit **************************/
// reverse a list
function reverse(l) = let(ll = len(l)) (ll > 0 ? [for (i = [1:ll]) l[ll-i]] : l);

// return elements indexed by indices in v
function select(v, indices) = [for (i = indices) v[i]];

// replace elements in v0 indexed by (sorted list) is with the elements in v1
function replace(v0, is, v1) = [for (i0 = 0, i1 = 0, rep = is[0] == 0;
									 i0 < len(v0);
									 i0 = i0+1, i1 = i1+(rep ? 1 : 0), rep = i0 == is[i1])
								rep ? v1[i1] : v0[i0]];

// cumulative sum of values in v
function cumsum(v) = [for (a = v[0]-v[0], i = 0; i < len(v); a = a+v[i], i = i+1) a+v[i]];

// sum values in v
function sum(v, i0 = 0, i1 = -1) =
	let(i = (i0 < 0 ? 0 : i0), n = (i1 < 0 ? len(v) : i1+1))
		len(v) > 0 ? _sum_rec(v, n, i+1, v[i]) : 0;
function _sum_rec(v, n, i, _acc) = i >= n ? _acc : _sum_rec(v, n, i+1, _acc+v[i]);

// substr, final char will be the one at i1-1
function substr(s, i0, i1, _acc = "") =
	i0 >= i1 ? _acc : substr(s, i0+1, i1, str(_acc, s[i0]));

// split string s at all c's
function splitstr(s, c = " ") = let(idx = concat(-1, search(c, s, 0)[0], len(s)))
	len(idx) > 2 ? [for (i = [1:len(idx)-1]) substr(s, idx[i-1]+1, idx[i])] : [];

// bounded string compare
function strncmp(s1, s2, n, s = 0) = len(s1) < n+s || len(s2) < n+s ? false :
	[for (i = [s:(s+n-1)]) s1[i] == s2[i]] == [for (_ = [s:(s+n-1)]) true];

// convenience function, summing a column in a matrix
function sum_col(m, c, start_i = 0, end_i = -1) = sum([for (v = m) v[c]], start_i, end_i);

// (first) index of min x value
function minxi(v) = let(x = [for(p = v) p.x]) search(min(x), x)[0];

// (first) index of max x value
function maxxi(v) = let(x = [for(p = v) p.x]) search(max(x), x)[0];

// min y value
function miny(v) = min([for(p = v) p.y]);

// (first) index of min y value
function minyi(v) = let(y = [for(p = v) p.y]) search(min(y), y)[0];

// ordered rotation, both arguments gotta be 3 element vectors
module ordered_rot(a, o) {
	function oi(i) = [for (ai = [0:2]) (o[i] == ai ? a[ai] : 0)];
	rotate(oi(2)) rotate(oi(1)) rotate(oi(0)) children();
}

// t-0.001 mm thick outline of children (the outline is 0.001 mm too small as well)
module projection_outline(t) {
	difference() {
		offset(r = -0.001) projection() children(0);
		offset(r = -t) projection() children(0);
	}
}

/************************** vaguely (mostly 2D) path related **************************/
// this is basically a watered down version of dotSCAD's shape_path_extend. the
// reason i wrote it is that dotSCAD's version generates points in a weird order,
// some points might even have the same coords. so with this quick'n'dirty function
// you at least know that a point on the top side with index i has it's buddy on
// the bottom side at 2*N-i-1, where N = len(pts). Note, thus, that for a path that
// starts in origo and propagates along the x-axis the first point in the returned
// path will be [0, -w/2] and the last will be [0, w/2], i.e. the "top" layer will
// have increasing indices in negative x-direction.
function stroke2d(pts, w = 1) = let(lp = len(pts)-1) _split_stroke(concat(
		[for (i = [0:(lp-1)]) _stroke_pts(pts[i], angle_of(pts[i], pts[i+1]), w/2)],
		[_stroke_pts(pts[lp], angle_of(pts[lp-1], pts[lp]), w/2)]
	));
function _stroke_pts(p, a, d) = let(c = cos(a+90), s = sin(a+90)) (
		[[p.x-c*d, p.y-s*d], [p.x+c*d, p.y+s*d]]
	);
function _split_stroke(s) = let(ls = len(s)) (
		concat([for (p = s) p[0]], [for (i = [1:ls]) s[ls-i][1]])
	);

// 2D arc path, another watered down version of a dotSCAD function (arc_path) but
// with the added features of being able to take two radii (to make an ellipse
// arc) and a position arg, such that your arc can start or be centered in origo. 
// note that the points are angularly equispaced, i.e. the point density will be
// higher around the narrower ends of an ellipse.
function arc_path2d(radius, angle, n = 30, pos = "") =
	let(r = (is_num(radius) ? [radius, radius] : radius),
		a = (is_num(angle) ? [0, angle] : angle),
		da = (a[1]-a[0])/(n-1),
		dr = (pos == "zero"   ? [r[0]*cos(a[0]), r[1]*sin(a[0])] :
			 (pos == "center" ? [r[0]*cos(a[0]+da*(n-1)/2), r[1]*sin(a[0]+da*(n-1)/2)] :
								[0, 0])))
		[for (i = [0:(n-1)]) [r[0]*cos(a[0]+i*da), r[1]*sin(a[0]+i*da)]-dr];

// the simplest line path
function line_path2d(length, n = 30) = let(dl = length/(n-1)) (
		[for (i = [0:(n-1)]) [i*dl, 0]]
	);

// and the simplest linesegment
function lineseg_path2d(p0,p1, n = 30) =
	let(dl = l2_dist(p0,p1)/(n-1),
		a = atan2(p1.y-p0.y, p1.x-p0.x),
		cdl = cos(a)*dl, sdl = sin(a)*dl) (
		[for (i = [0:(n-1)]) p0+[cdl*i, sdl*i]]
	);

// rectangle path with sides 2*sz around point p, rotated a degrees
function rect_path2d(p, sz, a = 0, open = true) =
	let(s = (is_num(sz) ? [sz, sz] : sz),
		pts = [for (c = [[1,-1], [1,1], [-1,1], [-1,-1]])
				p_rrot2d([c.x*s.x, c.y*s.y], a, p)])
		open ? pts : concat(pts, [pts[0]]);

// path where each point is [x, f(x)]
function func_path2d(f, l = 10, n_points = 0, x_density = 10) = 
	assert(n_points > 0 || x_density > 0, "ERROR: You must either provide n_points or x_density")
	let(dx = n_points > 0 ? l/(n_points-1) : l/(l*x_density-1))
		[for (x = [0:dx:l]) [x, f(x)]];

// linearly interpolate points in existing path, pts, such that the returned path
// contains n points. the points can be distributed in two different ways, keeping
// the inner points or not. this is controlled by the (optional) mode argument:
// mode = "seqd", semi equidistant (default): distribute points equidistantly over
//	the path but discard the interpolated points closest to existing inner ones,
//	i.e. all inner points are kept but the points in the resulting path are not
//	perfectly equidistantly distributed. Good for paths with sharp corners.
// mode = "eqd", equidistant: as seqd but discard the existing points instead of the
//	interpolated ones, i.e. points in the resulting path are all equidistant but the
//	"corners" of the path are most likely locally distorted. Good for smooth paths.
// the function does not cover all edge cases that come with interpolation and is
// not suitable for resampling measured curves.
function interp_path2d(pts, n, mode = "seqd") =
	let(lsl = [for (i = [1:len(pts)-1]) l2_dist(pts[i-1], pts[i])],	// lnseg lengths
		luk = concat([0], cumsum(lsl)/sum(lsl)),					// lookup keys
		lux = [for (i = [0:len(pts)-1]) [luk[i], pts[i].x]],		// lookup vals for x
		luy = [for (i = [0:len(pts)-1]) [luk[i], pts[i].y]],		// lookup vals for y
		plu = function(dl) [lookup(dl, lux), lookup(dl, luy)],		// point lookup func
		np = n-1)
		mode == "eqd"  ? [for (i = [0:np]) plu(i/np)]						  :
		mode == "seqd" ? replace([for (i = [0:np]) plu(i/np)],					// eqd
								 [for (i = [1:len(luk)-2]) round(luk[i]*np)],	// closest indices
								 [for (i = [1:len(pts)-1]) pts[i]]) 		  :	// "inner" points
						 assert(false, str(mode, " is not a supported interpolation mode"));

// make path pts start at o (default: [0,0]) and be directed towards u (default: no change)
function rebase_path2d(pts, u = [], o = [0, 0]) =
	len(u) == 2 ?
		let(da = angle2_of(o, u) - angle2_of(pts[0], pts[1]))
			[for (p = pts) p_rot2d(o+p-pts[0], da)] :
		[for (p = pts) o+p-pts[0]];

// quick'n'dirty bezier curve (in arbitrary number of dims tho), takes stepsize (dt,
// should preferably be 1 divided by an integer, if dt > 1 calamities ensue) and
// control points as args
function bezier_curve(dt, cps) = let(n = len(cps)-1, bc = _bez_bin_coef(n))
	[for (t = [0:dt:1]) [for (i = [0:n]) bc[i] * (1-t)^(n-i) * t^i]*cps];
function _bez_bin_coef(n) = // fastest way to compute binomial coeffs is to not.
	let(bc = [                                 [1],
                                              [1,1],
                                             [1,2,1],
                                            [1,3,3,1],
                                           [1,4,6,4,1],
                                         [1,5,10,10,5,1],
                                       [1,6,15,20,15,6,1],
                                      [1,7,21,35,35,21,7,1],
                                    [1,8,28,56,70,56,28,8,1],
                                  [1,9,36,84,126,126,84,36,9,1],
                              [1,10,45,120,210,252,210,120,45,10,1],
                            [1,11,55,165,330,462,462,330,165,55,11,1],
                          [1,12,66,220,495,792,924,792,495,220,66,12,1],
                      [1,13,78,286,715,1287,1716,1716,1287,715,286,78,13,1],
                  [1,14,91,364,1001,2002,3003,3432,3003,2002,1001,364,91,14,1],
               [1,15,105,455,1365,3003,5005,6435,6435,5005,3003,1365,455,105,15,1],
           [1,16,120,560,1820,4368,8008,11440,12870,11440,8008,4368,1820,560,120,16,1],
       [1,17,136,680,2380,6188,12376,19448,24310,24310,19448,12376,6188,2380,680,136,17,1],
    [1,18,153,816,3060,8568,18564,31824,43758,48620,43758,31824,18564,8568,3060,816,153,18,1],
[1,19,171,969,3876,11628,27132,50388,75582,92378,92378,75582,50388,27132,11628,3876,969,171,19,1]
	]) n < len(bc) ? bc[n] : [for (k = [0:n]) binomial_coef(n, k)];

/************************** a little more mathy stuff **************************/
// binomial coefficient (used by bezier_curve if there are more than 20 control points)
function binomial_coef(n, k) = (k == 0 || k == n) ? 1 : binomial_coef(n, k-1)*(n+1-k)/k;

// atan angle of line segment p0p1
function angle_of(p0, p1) = atan((p1.y-p0.y)/(p1.x-p0.x));
// convenience function, angle at point-index i in list of points pts
function angle_at(pts, i) = let(lp = len(pts)-1)
	assert((-1 < i) && (i < len(pts)), "index out of bounds") (
		((0 < i) && (i < lp)) ? angle_of(pts[i-1], pts[i+1]) :
				((i == 0) ? angle_of(pts[0], pts[1]) :
							angle_of(pts[lp-1], pts[lp])
				)
	);
// atan2 of line segment p0p1
function angle2_of(p0, p1) = atan2(p1.y-p0.y, p1.x-p0.x);

// angle over center point, cp, connected to points p0 and p1
function vangle(p0, p1, cp = [0,0]) = let(v0 = p0-cp, v1 = p1-cp) (
		acos((v0 * v1) / (norm(v0) * norm(v1)))
	);

// rotate a 2D point, p, a degrees around around another point, v
function p_rot2d(p, a, v = [0,0]) = let(x = p.x-v.x, y = p.y-v.y, c = cos(a), s = sin(a)) (
		v + [x*c - y*s, x*s + y*c]
	);
// convenience function, same as above but with the point being rotated given in coords
// relative to the point it's being rotated around
function p_rrot2d(p, a, v = [0,0]) = p_rot2d([v.x+p.x, v.y+p.y], a, v);

/* the following could of course be done with rotation matrices but when i tested      *
 * it that was a bit slower than doing it this way (also, i only use these rotations). */
// rotate point in 3D around x axis
function p_xrot3d(p, a) = let(c = cos(a), s = sin(a)) (
		[p.x, p.y*c - p.z*s, p.y*s + p.z*c]
	);
// rotate 3D point, p, a.x around the x and then a.y around y axis
function p_xyrot3d(p, a) = let(pr = p_xrot3d(p, a.x), c = cos(a.y), s = sin(a.y)) (
		[pr.x*c + pr.z*s, pr.y, -pr.x*s + pr.z*c]
	);
// rotate 3D point, p, a.x around the x axis, then a.y around y, and then a.z around z
function p_xyzrot3d(p, a) = let(pr = p_xyrot3d(p, a), c = cos(a.z), s = sin(a.z)) (
		[pr.x*c - pr.y*s, pr.x*s + pr.y*c, pr.z]
	);
// well, i needed this one a lot later than the ones above..
function p_ordered_rot3d(p, a, o = [0, 1, 2]) =
	let(f = [function (p,a) let(c=cos(a), s=sin(a)) [p.x, p.y*c-p.z*s, p.y*s+p.z*c],
			 function (p,a) let(c=cos(a), s=sin(a)) [p.x*c+p.z*s, p.y, p.z*c-p.x*s],
			 function (p,a) let(c=cos(a), s=sin(a)) [p.x*c-p.y*s, p.x*s+p.y*c, p.z]])
		f[o[2]]( f[o[1]]( f[o[0]](p, a[o[0]]), a[o[1]] ), a[o[2]] );

// this one just returns whichever scalar of a and b is closer to c (could extended to 2,3D)
function closer(a, b, c) = abs(c-b) < abs(c-a) ? b : a;

// l2 distance
function l2_dist(p0, p1 = [0,0]) = norm([p0.x-p1.x, p0.y-p1.y]);
// "signed" l2 distance between p1 and p2 is negative if p2 is closer to p0 than p1 is (i
// use it to check for overlap when placing caps).
function l2_sigdist(p0, p1, p2) = let(d = l2_dist(p1, p2)) (
		l2_dist(p0, p1) > l2_dist(p0, p2) ? -1*d : d
	);

// distance between a linesegment, p0p1, and a point, p2
function lnseg2p_dist(p0,p1, p2) = let(u = ((p2-p0)*(p1-p0))/((p1.y-p0.y)^2+(p1.x-p0.x)^2)) (
		(u < 0 ? l2_dist(p0,p2) :
		(u > 1 ? l2_dist(p1,p2) :
				 l2_dist(p0+u*(p1-p0), p2)))
	);

// (smallest) distance between two rectangles. rectangle representation is just a list of
// the four corners, [[x0,y0], ..., [x3,y3]]. whenever i get the time i'll implement
// something smart here.. but for now i'll just take the min of all points to all line
// segments. this, of course, assumes that the rects do not overlap (see below).
function rect2rect_dist(r0, r1) = min(concat(
		[for (p = r0, ls = [[r1[0], r1[1]], [r1[1], r1[2]], [r1[2], r1[3]], [r1[3], r1[0]]])
			lnseg2p_dist(ls[0],ls[1], p)],
		[for (p = r1, ls = [[r0[0], r0[1]], [r0[1], r0[2]], [r0[2], r0[3]], [r0[3], r0[0]]])
			lnseg2p_dist(ls[0],ls[1], p)]
	));

// check if two rects overlap (separating axis theorem). rectangle representation is
// [[x, y], [w/2, h/2], a], where x,y are the coords of the center of the rect. so SAT for
// rectangles is basically just projecting the vector from one center point to the other
// onto the four unit vectors (along the two dims of each rect) and comparing the length of
// it with the projection of each (half)rectangle onto the same axes. this sounds like
// alotta projections but since we'll be projecting a rectangle onto one of its own axes
// each time, that projection will simply be the (half)width or (half)height of it, and the
// other 0 - so there are really only three projections needed per axis.
function rects_overlap(r0, r1) =
	let(T = r1[0] - r0[0],
		Ax_r0 = [cos(r0[2]), sin(r0[2])], Ay_r0 = [-1*Ax_r0[1], Ax_r0[0]], // r0 unit vectors (axes)
		Ax_r1 = [cos(r1[2]), sin(r1[2])], Ay_r1 = [-1*Ax_r1[1], Ax_r1[0]], // r1 unit vectors
		w0Ax = r0[1][0]*Ax_r0, h0Ay = r0[1][1]*Ay_r0,	// r0 projections
		w1Ax = r1[1][0]*Ax_r1, h1Ay = r1[1][1]*Ay_r1) (	// r1 projections
		!(abs(proj_1d(T, Ax_r0)) > (r0[1][0]+abs(proj_1d(w1Ax, Ax_r0))+abs(proj_1d(h1Ay, Ax_r0))) ||
		  abs(proj_1d(T, Ay_r0)) > (r0[1][1]+abs(proj_1d(w1Ax, Ay_r0))+abs(proj_1d(h1Ay, Ay_r0))) ||
		  abs(proj_1d(T, Ax_r1)) > (r1[1][0]+abs(proj_1d(w0Ax, Ax_r1))+abs(proj_1d(h0Ay, Ax_r1))) ||
		  abs(proj_1d(T, Ay_r1)) > (r1[1][1]+abs(proj_1d(w0Ax, Ay_r1))+abs(proj_1d(h0Ay, Ay_r1))))
	);

// scalar projection of vector v onto axis A, i.e. norm(v)*cos(vangle(v,A)) or:
function proj_1d(v, A) = (v*A) / norm(A);

// cross product of vectors p0p1 and p0p2
function xprod(p0, p1, p2) = cross([p1.x-p0.x, p1.y-p0.y], [p2.x-p0.x, p2.y-p0.y]);

// check if line segments p0p1 and p2p3 intersect, i never remember this so i wrote it down:
// if sgn(p0p1 x p0p2) == sgn(p0p1 x p0p3) then p0p1 and p2p3 does not intersect (p2, p3 are
// on the same side of p0p1). if sgn(p0p1 x p0p2) != sgn(p0p1 x p0p3) we know p0p1 and p2p3
// may intersect (if they're inf. long they will). to make sure they really do we need to
// check that sgn(p2p3 x p2p0) != sgn(p2p3 x p2p0) is also true, which means that p2.x, p3.x
// are in between p0.x and p1.x
function intersects(p0,p1, p2,p3) = (sign(xprod(p0, p1, p2)) != sign(xprod(p0, p1, p3))) &&
									(sign(xprod(p2, p3, p0)) != sign(xprod(p2, p3, p1)));

// return the point of intersection for the line segments p0p1 and p2p3 (the universe will
// implode if the line segments do not intersect)
function intersection_pt(p0,p1, p2,p3) =
	let(a0 = p1.y-p0.y, b0 = p0.x-p1.x, c0 = a0*p0.x+b0*p0.y,
		a1 = p3.y-p2.y, b1 = p2.x-p3.x, c1 = a1*p2.x+b1*p2.y,
		det = a0*b1-a1*b0) ( // cross([a0, a1], [b0, b1]
		[(b1*c0-b0*c1)/det, (a0*c1-a1*c0)/det]
	);

// extend the line segment p0p1 to reach y (and return that coord) 
function extend_p0p1_to_y(p0,p1, y) = [p0.x + (p0.y - y)*(p0.x - p1.x)/(p1.y - p0.y), y];

// find point pX on p0p1 that is located at a distance d away from p2 (law of sines). note
// that if there are two points on p0p1 that are d away from p2 the one that is furthest
// away from the one of p0 and p1 that is closest to p2 will be returned. should p0 and p1
// be at the same distance from p2, then pX will be the point furthest away from p0.
function p_on_p0p1(p0,p1, p2, d) =
	let(p = (l2_dist(p0, p2) <= l2_dist(p1, p2) ? [p0, p1] : [p1, p0]),
		a0 = vangle(p[1], p2, p[0]),
		sa0 = sin(a0),
		saX = l2_dist(p[0], p2)*sa0/d, // here could be rounding errors
		l_p0pX = sin(180-a0-(saX < 1 ? asin(saX) : 90)) * d/sa0,
		a = atan2(p[1].y-p[0].y, p[1].x-p[0].x)) (
		assert(0 < l_p0pX && l_p0pX < l2_dist(p0,p1), "No point on p0p1 is d away from p2")
		[p[0].x+cos(a)*l_p0pX, p[0].y+sin(a)*l_p0pX]
	);


/************************** debugging shenanigans **************************/
// hull line
module hl(pts, w = 1, dots = true, dot_r = 0.1, dot_clr = "red", $fn = 20) {
	if (is_num(pts[0])) _1d_hl(pts, w, dots, dot_r, dot_clr);
	else if (len(pts[0]) == 2) _2d_hl(pts, w, dots, dot_r, dot_clr);
	else _3d_hl(pts, w, dots, dot_r, dot_clr);
}
module _1d_hl(pts, w, dots, dot_r, dot_clr) {
	hull() {
		translate([pts[0], 0]) circle(w/2);
		translate([pts[len(pts)-1], 0]) circle(w/2);
	}
	if (dots) for (px = pts) color(dot_clr) translate([px, 0]) circle(dot_r);
}
module _2d_hl(pts, w, dots, dot_r, dot_clr) {
	for (i = [1:len(pts)-1]) {
		hull() {
			translate(pts[i-1]) circle(w/2);
			translate(pts[i]) circle(w/2);
		}
		if (dots) color(dot_clr) translate(pts[i-1]) circle(dot_r);
	}
	if (dots) color(dot_clr) translate(pts[len(pts)-1]) circle(dot_r);
}
module _3d_hl(pts, w, dots, dot_r, dot_clr) {
	for (i = [1:len(pts)-1]) {
		hull() {
			translate(pts[i-1]) sphere(w/2); // "module literals" would be nice..
			translate(pts[i]) sphere(w/2);
		}
		if (dots) color(dot_clr) translate(pts[i-1]) sphere(dot_r);
	}
	if (dots) color(dot_clr) translate(pts[len(pts)-1]) sphere(dot_r);
}

// just a short hand for linear_extrude(height = h)
module le(h = 1) {
	linear_extrude(height = h) children();
}
